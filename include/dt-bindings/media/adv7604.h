/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __DT_BINDINGS_MEDIA_ADV76XX_H__
#define __DT_BINDINGS_MEDIA_ADV76xx_H__

/* Analog input muxing modes (AFE register 0x02, [2:0]) */
#define	ADV7604_AIN1_2_3_NC_SYNC_1_2	0
#define	ADV7604_AIN4_5_6_NC_SYNC_2_1	1
#define	ADV7604_AIN7_8_9_NC_SYNC_3_1	2
#define	ADV7604_AIN10_11_12_NC_SYNC_4_1	3
#define	ADV7604_AIN9_4_5_6_SYNC_2_1	4

/*
 * Bus rotation and reordering. This is used to specify component reordering on
 * the board and describes the components order on the bus when the ADV7604
 * outputs RGB.
 */
#define	ADV7604_BUS_ORDER_RGB	0	/* No operation	*/
#define	ADV7604_BUS_ORDER_GRB	1	/* Swap 1-2	*/
#define	ADV7604_BUS_ORDER_RBG	2	/* Swap 2-3	*/
#define	ADV7604_BUS_ORDER_BGR	3	/* Swap 1-3	*/
#define	ADV7604_BUS_ORDER_BRG	4	/* Rotate right	*/
#define	ADV7604_BUS_ORDER_GBR	5	/* Rotate left	*/

#endif /* __DT_BINDINGS_MEDIA_XILINX_VIP_H__ */
