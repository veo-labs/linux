/*
 * LMH0318 -- Texas Instrument SDI reclocker driver.
 * Copyright (C) 2016  Jean-Michel Hautbois
 *
 * 3G HD/SD SDI Reclocker with Integrated Cable Driver
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/i2c.h>
#include <linux/ioctl.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/regmap.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/videodev2.h>
#include <media/v4l2-device.h>
#include <media/v4l2-of.h>

#define LMH0318_PAD_IN0		0
#define	LMH0318_PAD_IN1		1
#define LMH0318_PAD_OUT0	2
#define LMH0318_PAD_OUT1	3
#define	LMH0318_PADS_NUM	4

struct lmh0318_state {
	struct v4l2_subdev sd;
	struct i2c_client *i2c_client;
	struct media_pad pads[LMH0318_PADS_NUM];
	struct regmap *regmap;
	unsigned gpio;
	int irq;
	bool current_state;
	u32 heo;
	u32 veo;
};

static const struct regmap_config lmh0318_regmap = {
	.name			= "lmh0318",
	.reg_bits		= 8,
	.val_bits		= 8,
	.max_register		= 0xff,
	.cache_type		= REGCACHE_NONE,
};

static inline struct lmh0318_state *to_state(struct v4l2_subdev *sd)
{
	return container_of(sd, struct lmh0318_state, sd);
}

static int lmh0318_log_status(struct v4l2_subdev *sd)
{
	struct lmh0318_state *state = to_state(sd);
	int ret;

	v4l2_info(sd, "-----Chip status-----\n");
	regmap_read(state->regmap, 0xf0, &ret);
	v4l2_info(sd, "Device lmh0318 version %#x\n", ret);

	return 0;
}

static int raw_op(struct lmh0318_state *state, u8 reg, u8 value, u8 mask)
{
	int val;
	regmap_read(state->regmap, reg, &val);
	val = (val & ~mask) | value;
	regmap_write(state->regmap, reg, val);

	return 0;
}

static int lmh0318_reset_cdr(struct lmh0318_state *state)
{
	/* Reset CDR */
	raw_op(state, 0x0a, 0x0c, 0x0c);
	/* Release Reset */
	raw_op(state, 0x0a, 0x0, 0x0c);

	return 0;
}

static int lmh0318_init(struct lmh0318_state *state)
{
	/* Initialization sequence */
	/* Enable channel registers */
	/* Set LOS_INT_N in interrupt mode */
	raw_op(state, 0xff, 0x04, 0x07);
	/* Enable full temperature range */
	raw_op(state, 0x16, 0x25, 0xff);
	/* Initialize CDR State Machine Control */
	raw_op(state, 0x3e, 0x00, 0x80);
	raw_op(state, 0x55, 0x02, 0x02);
	raw_op(state, 0x6a, 0x00, 0xff);
	/* Restore media CTLE Setting */
	raw_op(state, 0x03, 0x00, 0xff);

	lmh0318_reset_cdr(state);

	return 0;
}

static int lmh0318_common_conf(struct lmh0318_state *state)
{
	int los_in0;

	regmap_read(state->regmap, 0x1, &los_in0);
	if (los_in0 & 0x01)
		dev_dbg(&state->i2c_client->dev, "Signal is present in IN0\n");
	/* Enable IN0 @ 75 ohms and power down 50 ohms IN1 */
	raw_op(state, 0x31, 0x1, 0x3);
	/* Default SMPTE enabled */
	raw_op(state, 0x2f, 0x0, 0xc0);

	lmh0318_reset_cdr(state);

	return 0;
}

static int lmh0318_read_eom(struct lmh0318_state *state)
{
	/* Select Channel Registers */
	raw_op(state, 0xff, 0x04, 0x07);
	/* Enable EOM */
	raw_op(state, 0x11, 0x00, 0x20);
	/* Enable HEO/VEO */
	raw_op(state, 0x3e, 0x80, 0x80);
	/* Read HEO, convert hex to dec, then divide by 64 for value in UI */
	regmap_read(state->regmap, 0x27, &state->heo);
	/* Read VEO, convert hex to decimal and Multiply by 3.125mV */
	regmap_read(state->regmap, 0x28, &state->veo);
	dev_dbg(&state->i2c_client->dev, "HEO : %#x, VEO : %#x", state->heo, state->veo);
	state->heo = (state->heo * 100) / 64;
	state->veo = (state->veo * 3125) / 1000;
	dev_dbg(&state->i2c_client->dev, "HEO : %u%% UI, VEO : %umV", state->heo, state->veo);
	/* Restore initialization setting */
	raw_op(state, 0x3e, 0x00, 0x80);
	/* Power down EOM */
	raw_op(state, 0x11, 0x20, 0x20);

	lmh0318_reset_cdr(state);

	return 0;
}

static irqreturn_t lmh0318_irq(int irq, void *devid)
{
	int reg;
	struct lmh0318_state *state = devid;

	/* Get signal status */
	regmap_read(state->regmap, 0x01, &reg);
	state->current_state = !(reg & 0x01);

	/* Clear ISR register */
	regmap_read(state->regmap, 0x54, &reg);

	if (state->current_state) {
		/* Signal detected, wait for loss of signal */
		regmap_write(state->regmap, 0x56, 0x01);
		lmh0318_read_eom(state);
	} else {
		/* no signal, wait for signal */
		regmap_write(state->regmap, 0x56, 0x10);
	}

	dev_dbg(&state->i2c_client->dev, "carrier %s", state->current_state ? "detected" : "off");

	/* Force back IRQ */
	raw_op(state, 0xff, 0x24, 0x07);
	lmh0318_reset_cdr(state);

	return IRQ_HANDLED;
}

#ifdef CONFIG_VIDEO_ADV_DEBUG
static int lmh0318_g_register(struct v4l2_subdev *sd,
					struct v4l2_dbg_register *reg)
{
	int ret;
	struct lmh0318_state *state = to_state(sd);

	regmap_read(state->regmap, reg->reg, &ret);
	if (ret < 0) {
		v4l2_info(sd, "Register %03llx not supported\n", reg->reg);
		return ret;
	}

	reg->size = 1;
	reg->val = ret;

	return 0;
}

static int lmh0318_s_register(struct v4l2_subdev *sd,
				const struct v4l2_dbg_register *reg)
{
	int ret;
	struct lmh0318_state *state = to_state(sd);

	ret = regmap_write(state->regmap, reg->reg, reg->val);
	if (ret < 0) {
		v4l2_info(sd, "Register %03llx not supported\n", reg->reg);
		return ret;
	}

	return 0;
}
#endif

static int lmh0318_registered(struct v4l2_subdev *sd)
{
	struct lmh0318_state *state = to_state(sd);
	int reg;

	/* Get signal status */
	regmap_read(state->regmap, 0x01, &reg);
	state->current_state = !(reg & 0x01);
	dev_dbg(&state->i2c_client->dev, "Signal at boot is %s\n", state->current_state ? "detected" : "off");

	/* Clear ISR register */
	regmap_read(state->regmap, 0x54, &reg);

	if (state->current_state) {
		/* Signal detected, wait for loss of signal */
		regmap_write(state->regmap, 0x56, 0x01);
		/* Read EOM at boot */
		lmh0318_read_eom(state);
	} else {
		/* no signal, wait for signal */
		regmap_write(state->regmap, 0x56, 0x10);
	}

	/* Ask for an interrupt when IN0 locks */
	raw_op(state, 0xff, 0x20, 0x20);
	lmh0318_reset_cdr(state);

	dev_dbg(&state->i2c_client->dev, "LMH0318 is registered\n");

	return 0;
}

static const struct v4l2_subdev_internal_ops lmh0318_internal_ops = {
	.registered = lmh0318_registered,
};

static const struct v4l2_subdev_core_ops lmh0318_core_ops = {
	.log_status = lmh0318_log_status,
#ifdef CONFIG_VIDEO_ADV_DEBUG
	.g_register = lmh0318_g_register,
	.s_register = lmh0318_s_register,
#endif

};

static const struct v4l2_subdev_ops lmh0318_ops = {
	.core = &lmh0318_core_ops,
};


static int lmh0318_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	struct v4l2_subdev *sd;
	struct lmh0318_state *state;
	int ret = 0;

	/* Check if the adapter supports the needed features */
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_SMBUS_BYTE_DATA))
		return -EIO;

	state = devm_kzalloc(&client->dev, sizeof(*state), GFP_KERNEL);
	if (state == NULL)
		return -ENOMEM;

	if (client->dev.of_node) {
		struct device_node *n = NULL;
		struct of_endpoint ep;

		dev_dbg(&client->dev, "Parsing DT configuration\n");
		while ((n = of_graph_get_next_endpoint(client->dev.of_node, n))
								!= NULL) {
			ret = of_graph_parse_endpoint(n, &ep);
			if (ret < 0) {
				dev_err(&client->dev, "Could not parse endpoint: %d\n", ret);
				of_node_put(n);
				return ret;
			}
			dev_dbg(&client->dev, "endpoint %d on port %d\n",
						ep.id, ep.port);
			of_node_put(n);
		}
	} else {
		dev_dbg(&client->dev, "No DT configuration\n");
	}

	/* Configure regmap */
	state->regmap =	devm_regmap_init_i2c(client,
					&lmh0318_regmap);
	if (IS_ERR(state->regmap)) {
		ret = PTR_ERR(state->regmap);
		dev_err(&client->dev,
			"Error initializing regmap with error %d\n",
			ret);
		return -EINVAL;
	}

	state->i2c_client = client;

	lmh0318_init(state);
	lmh0318_common_conf(state);

	if (client->dev.of_node) {
		/* Configure PROGRAMN pin */
		ret = of_get_named_gpio(client->dev.of_node, "gpios", 0);
		if (ret < 0)
			dev_dbg(&client->dev, "No interrupt pin defined\n");

		if (gpio_is_valid(ret)) {
			state->gpio = ret;
			ret = devm_gpio_request(&client->dev, ret, "LMH0318-IRQ");
			gpio_direction_input(state->gpio);
			state->irq = client->irq;
			if (client->irq) {
				ret = request_threaded_irq(client->irq, NULL, lmh0318_irq,
							   IRQF_ONESHOT | IRQF_TRIGGER_FALLING,
							   KBUILD_MODNAME, state);
				if (ret)
					goto unreg_i2c;
			}
		}
		else {
			dev_err(&client->dev, "LMH0318 INT_N gpio is not valid\n");
		}
	}

	/* subdev init */
	sd = &state->sd;
	v4l2_i2c_subdev_init(sd, client, &lmh0318_ops);
	sd->internal_ops = &lmh0318_internal_ops;
	strlcpy(sd->name, "LMH0318 SDI reclocker", sizeof(sd->name));
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;

	/* Media Controller init */
	state->pads[LMH0318_PAD_IN0].flags = MEDIA_PAD_FL_SINK;
	state->pads[LMH0318_PAD_IN1].flags = MEDIA_PAD_FL_SINK;
	state->pads[LMH0318_PAD_OUT0].flags = MEDIA_PAD_FL_SOURCE;
	state->pads[LMH0318_PAD_OUT1].flags = MEDIA_PAD_FL_SOURCE;

	ret = media_entity_init(&state->sd.entity, LMH0318_PADS_NUM, state->pads, 0);
	if (ret < 0) {
		dev_err(&client->dev, "media entity failed with error %d\n", ret);
		goto unreg_subdev;
	}

	ret = v4l2_async_register_subdev(sd);
	if (ret < 0) {
		goto unreg_media;
	}

	return 0;

unreg_media:
	media_entity_cleanup(&sd->entity);
unreg_subdev:
	v4l2_device_unregister_subdev(sd);
	if (client->irq)
		free_irq(client->irq, state);
unreg_i2c:
	i2c_unregister_device(client);
	return ret;
}

static int lmh0318_remove(struct i2c_client *client)
{
	struct v4l2_subdev *sd = i2c_get_clientdata(client);
	struct lmh0318_state *state = to_state(sd);

	sd = &state->sd;
	media_entity_cleanup(&sd->entity);
	v4l2_device_unregister_subdev(sd);
	if (client->irq)
		free_irq(client->irq, state);
	i2c_unregister_device(client);

	return 0;
}

/* ----------------------------------------------------------------------- */

static const struct i2c_device_id lmh0318_id[] = {
	{ "lmh0318", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, lmh0318_id);

static const struct of_device_id lmh0318_of_id[] __maybe_unused = {
	{ .compatible = "ti,lmh0318" },
	{ /* sentinel */ },
};
MODULE_DEVICE_TABLE(of, lmh0318_of_id);

static struct i2c_driver lmh0318_driver = {
	.driver = {
		.owner	= THIS_MODULE,
		.name	= "lmh0318",
		.of_match_table = of_match_ptr(lmh0318_of_id),
	},
	.probe		= lmh0318_probe,
	.remove		= lmh0318_remove,
	.id_table	= lmh0318_id,
};

module_i2c_driver(lmh0318_driver);
MODULE_DESCRIPTION("Texas Instrument LMH0318 SDI reclocker");
MODULE_AUTHOR("Jean-Michel Hautbois <jean-michel.hautbois@veo-labs.com>");
MODULE_LICENSE("GPL");
