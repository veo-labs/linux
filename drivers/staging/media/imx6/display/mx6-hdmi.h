/*
 * HDMI Transmiter driver for Freescale i.MX6 SOC
 *
 * Copyright (c) 2015 Vodalys-Labs.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */

#ifndef _MX6_HDMI_H_
#define _MX6_HDMI_H_

#include <media/v4l2-subdev.h>

#define HDMI_EDID_LEN		512

/* Notify events to v4l2 bridge */
#define MX6DIS_HDMI_HOTPLUG		1

/***********************************
 * HDMI Structures and definitions
 ***********************************/
enum {
	IMX6_HDMI_RES_8,
	IMX6_HDMI_RES_10,
	IMX6_HDMI_RES_12,
	IMX6_HDMI_RES_MAX,
};

/* Structure for define the possibles CURRENT CONTROL values for defining
 * mpll and pll for a given pixelclock */
struct mx6dis_hdmi_curr_ctrl {
	unsigned long mpixelclock;
	u16 curr[IMX6_HDMI_RES_MAX];
};

struct mx6dis_hdmi_mpll_config {
	unsigned long mpixelclock;
	struct {
		u16 cpce;
		u16 gmp;
	} res[IMX6_HDMI_RES_MAX];
};

struct mx6dis_hdmi_sym_term {
	unsigned long mpixelclock;
	u16 sym_ctr;    /*clock symbol and transmitter control*/
	u16 term;       /*transmission termination value*/
};

struct mx6dis_hdmi_plat_data {
	const struct mx6dis_hdmi_mpll_config *mpll_cfg;
	const struct mx6dis_hdmi_curr_ctrl *cur_ctr;
	const struct mx6dis_hdmi_sym_term *sym_term;
};

struct mx6dis_hdmi_pixfmt {
	struct v4l2_pix_format pix_format;
	int    depth;
};

struct mx6dis_hdmi_state {
	struct device *dev;
	int irq;
	struct resource *iores;
	struct regmap *regmap;

	struct v4l2_subdev sd;

	struct clk *isfr_clk;
	struct clk *iahb_clk;

	struct v4l2_dv_timings timings;
	struct mx6dis_hdmi_pixfmt pixel_format;
	const struct mx6dis_hdmi_plat_data *plat_data;

	u8 edid[HDMI_EDID_LEN];
	bool cable_plugin;
	bool hdcp_enable;
	bool phy_enabled;

	struct i2c_adapter *ddc;
	void __iomem *regs;

	unsigned int sample_rate;
	int ratio;
	int vic; /* InfoFrame CEA Index */

	void (*write)(struct mx6dis_hdmi_state *hdmi, u8 val, int offset);
	u8 (*read)(struct mx6dis_hdmi_state *hdmi, int offset);
};

/* Supported CEA-861-E */
static const struct v4l2_dv_timings cea_861e_timings[] = {
	/* 1 DMT0659    4:3                  640x480p @ 59.94/60 Hz */
	{ },
	/* 2 480p       4:3     8:9          720x480p @ 59.94/60 Hz */
	{ },
	/* 3 480pH     16:9    32:37         720x480p @ 59.94/60 Hz */
	{ },
	/* 4. 720p 16:9 1:1 1280x720p @ 59.94/60 Hz */
	V4L2_DV_BT_CEA_1280X720P60,
	/* 5 1080i     16:9     1:1        1920x1080i @ 59.94/60 Hz */
	{ },
	/* 6 480i       4:3     8:9    720(1440)x480i @ 59.94/60 Hz */
	{ },
	/* 7 480iH     16:9    32:37   720(1440)x480i @ 59.94/60 Hz */
	{ },
	/* 8 240p       4:3     8:9    720(1440)x240p @ 59.94/60 Hz */
	{ },
	/* 9 240pH     16:9    32:37   720(1440)x240p @ 59.94/60 Hz */
	{ },
	/* 10 480i4x     4:3     8:9       (2880)x480i @ 59.94/60 Hz */
	{ },
	/* 11 480i4xH   16:9    32:37      (2880)x480i @ 59.94/60 Hz */
	{ },
	/* 12 240p4x     4:3     8:9       (2880)x240p @ 59.94/60 Hz */
	{ },
	/* 13 240p4xH   16:9    32:37      (2880)x240p @ 59.94/60 Hz */
	{ },
	/* 14 480p2x     4:3     8:9         1440x480p @ 59.94/60 Hz */
	{ },
	/* 15 480p2xH   16:9    32:37        1440x480p @ 59.94/60 Hz */
	{ },
	/* 16 1080p     16:9     1:1        1920x1080p @ 59.94/60 Hz */
	V4L2_DV_BT_CEA_1920X1080P60,
	/* 17 576p       4:3    16:15         720x576p @ 50 Hz */
	{ },
	/* 18 576pH     16:9    64:45         720x576p @ 50 Hz */
	{ },
	/* 19 720p50    16:9     1:1        1280x720p @ 50 Hz */
	V4L2_DV_BT_CEA_1280X720P50,
	/* 20 1080i25   16:9     1:1        1920x1080i @ 50 Hz* */
	{ },
	/* 21 576i       4:3    16:15   720(1440)x576i @ 50 Hz */
	{ },
	/* 22 576iH     16:9    64:45   720(1440)x576i @ 50 Hz */
	{ },
	/* 23 288p       4:3    16:15   720(1440)x288p @ 50 Hz */
	{ },
	/* 24 288pH     16:9    64:45   720(1440)x288p @ 50 Hz */
	{ },
	/* 25 576i4x     4:3    16:15      (2880)x576i @ 50 Hz */
	{ },
	/* 26 576i4xH   16:9    64:45      (2880)x576i @ 50 Hz */
	{ },
	/* 27 288p4x     4:3    16:15      (2880)x288p @ 50 Hz */
	{ },
	/* 28 288p4xH   16:9    64:45      (2880)x288p @ 50 Hz */
	{ },
	/* 29 576p2x     4:3    16:15        1440x576p @ 50 Hz */
	{ },
	/* 30 576p2xH   16:9    64:45        1440x576p @ 50 Hz */
	{ },
	/* 31 1080p50   16:9     1:1        1920x1080p @ 50 Hz */
	{ },
	/* 32 1080p24   16:9     1:1        1920x1080p @ 23.98/24 Hz */
	V4L2_DV_BT_CEA_1920X1080P24,
	/* 33 1080p25   16:9     1:1        1920x1080p @ 25 Hz */
	V4L2_DV_BT_CEA_1920X1080P25,
	/* 34 1080p30   16:9     1:1        1920x1080p @ 29.97/30 Hz */
	V4L2_DV_BT_CEA_1920X1080P30,
	/* 35 480p4x     4:3     8:9       (2880)x480p @ 59.94/60 Hz */
	{ },
	/* 36 480p4xH   16:9    32:37      (2880)x480p @ 59.94/60 Hz */
	{ },
	/* 37 576p4x    4:3     16:15      (2880)x576p @ 50 Hz */
	{ },
	/* 38 576p4xH   16:9    64:45      (2880)x576p @ 50 Hz */
	{ },
	/* 39 1080i25   16:9     1:1        1920x1080i @ 50 Hz* (1250 Total) */
	{ },
	/* 40 1080i50   16:9     1:1        1920x1080i @ 100 Hz */
	{ },
	/* 41 720p100   16:9     1:1         1280x720p @ 100 Hz */
	{ },
	/* 42 576p100    4:3     8:9          720x576p @ 100 Hz */
	{ },
	/* 43 576p100H  16:9    32:37         720x576p @ 100 Hz */
	{ },
	/* 44 576i50     4:3    16:15   720(1440)x576i @ 100 Hz */
	{ },
	/* 45 576i50H   16:9    64:45   720(1440)x576i @ 100 Hz */
	{ },
	/* 46 1080i60   16:9     1:1        1920x1080i @ 119.88/120 Hz */
	{ },
	/* 47 720p120   16:9     1:1         1280x720p @ 119.88/120 Hz */
	{ },
	/* 48 480p119    4:3    16:15         720x480p @ 119.88/120 Hz */
	{ },
	/* 49 480p119H  16:9    64:45         720x480p @ 119.88/120 Hz */
	{ },
	/* 50 480i59     4:3     8:9    720(1440)x480i @ 119.88/120 Hz */
	{ },
	/* 51 480i59H   16:9    32:37   720(1440)x480i @ 119.88/120 Hz */
	{ },
	/* 52 576p200    4:3    16:15         720x576p @ 200 Hz */
	{ },
	/* 53 576p200H  16:9    64:45         720x576p @ 200 Hz */
	{ },
	/* 54 576i100    4:3    16:15   720(1440)x576i @ 200 Hz */
	{ },
	/* 55 576i100H  16:9    64:45   720(1440)x576i @ 200 Hz */
	{ },
	/* 56 480p239    4:3     8:9          720x480p @ 239.76/240 Hz */
	{ },
	/* 57 480p239H  16:9    32:37         720x480p @ 239.76/240 Hz */
	{ },
	/* 58 480i119    4:3     8:9    720(1440)x480i @ 239.76/240 Hz */
	{ },
	/* 59 480i119H  16:9    32:37   720(1440)x480i @ 239.76/240 Hz */
	{ },
	/* 60 720p24    16:9     1:1         1280x720p @ 23.98/24 Hz */
	{ },
	/* 61 720p25    16:9     1:1         1280x720p @ 25Hz */
	{ },
	/* 62 720p30    16:9     1:1         1280x720p @ 29.97/30 Hz */
	{ },
	/* 63. 1080p120  16:9     1:1        1920x1080p @ 119.88/120 Hz */
	{ },
	/* 64 1080p100  16:9     1:1        1920x1080p @ 100 Hz */
	{ },
	{ },
};
#endif /* MX6_HDMI_H_ */
