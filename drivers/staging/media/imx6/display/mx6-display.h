/*
 * Video Display driver for Freescale i.MX6 SOC
 *
 * Copyright (c) 2015 Vodalys-Labs.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#ifndef _MX6_DISPLAY_H
#define _MX6_DISPLAY_H

#include <media/v4l2-dv-timings.h>

#define MX6DIS_EOF_TIMEOUT	1000

/* Pixel format structure and supported pixel formats */
struct mx6dis_pixfmt {
	char	*name;
	u32	fourcc;
	u32	codes[4];
	int	depth;	/* total bpp */
	int	ybpp;	/* depth of first Y plane for planar formats */
};

static struct mx6dis_pixfmt mx6dis_pixformats[] = {
	{
		.name	= "RGB24",
		.fourcc	= V4L2_PIX_FMT_RGB24,
		.codes	= {MEDIA_BUS_FMT_RGB888_1X24},
		.depth	= 24,
		.ybpp	= 3,
	},
};
#define NUM_FORMATS ARRAY_SIZE(mx6dis_pixformats)

struct mx6dis_buffer {
	struct vb2_buffer	vb; /* v4l buffer must be first */
	struct list_head	list;
};

struct mx6dis_dma_buf {
	void		*virt;
	dma_addr_t	phys;
	unsigned long	len;
};

struct mx6dis_dev {
	struct v4l2_device		v4l2_dev;
	struct video_device		vfd;
	struct device			*dev;

	/* Subdev related fields */
	struct mx6dis_hdmi_state	*mx6dis_hdmi;
	struct v4l2_async_notifier	notifier;
	struct v4l2_async_subdev	asd;
	int				current_output; /* 0 by default */

	struct mutex			mutex;
	spinlock_t			irqlock;

	/* buffer queue used in videobuf2 */
	struct vb2_queue		buffer_queue;
	/* List of buffers free for dequeuing */
	struct list_head		buf_list;
	/* Sequence number from stream on */
	unsigned int			sequence;
	void				*alloc_ctx;

	/* Display Interface index (0 or 1)*/
	int				di_id;
	/* ipu_soc device */
	struct ipu_soc			*ipu;

	/* v4l2 controls */
	struct v4l2_ctrl_handler	ctrl_hdlr;

	/* Configured input pixel format */
	struct v4l2_pix_format		input_fmt;
	/* Configured output pixel format */
	struct v4l2_pix_format		output_fmt;
	/* Driver specific information about output format */
	struct mx6dis_pixfmt		*display_pixfmt; /*After s_fmt_vid_out*/
	/*struct v4l2_mbus_framefmt display_framefmt;*/

	struct v4l2_dv_timings		mx6dis_timings;

	struct mx6dis_ctx		*ctx;
	bool				stream_on;
};

/* Structure for describing a specific context */
struct mx6dis_ctx {
	struct v4l2_fh		fh;
	struct mx6dis_dev	*dev;

	struct ipuv3_channel	*dis_ch;
	struct dmfc_channel	*dmfc;
	struct ipu_di		*di;
	struct ipu_dc		*dc;
	struct ipu_dp		*dp;

	struct v4l2_pix_format	in_fmt;	/* input pixel format */
	struct v4l2_pix_format	out_fmt;/* output pixel format */
	enum ipu_color_space	in_cs;  /* input colorspace */
	enum ipu_color_space	out_cs; /* output colorspace */

	/* active (undergoing DMA) buffers, one for each IDMAC buffer */
	struct mx6dis_buffer	*active_frame[2];

	struct mx6dis_dma_buf	underrun_buf;
	int			buf_num;

	struct timer_list	eof_timeout_timer;
	int			eof_irq;
	int			nfack_irq;

	bool			last_eof;  /* waiting for last EOF */
	struct completion	last_eof_comp;

	/* is this ctx allowed to do IO */
	bool			io_allowed;
};

/* Supported CEA-861-E */
static const struct v4l2_dv_timings mx6dis_timings[] = {
	/*V4L2_DV_BT_CEA_640X480P59_94,
	V4L2_DV_BT_CEA_720X480P59_94,
	V4L2_DV_BT_CEA_1280X720P60,
	V4L2_DV_BT_CEA_1920X1080I60,
	V4L2_DV_BT_CEA_1920X1080P60,
	V4L2_DV_BT_CEA_720X576P50,*/
	V4L2_DV_BT_CEA_1280X720P50,
	/*V4L2_DV_BT_CEA_1920X1080I50,
	V4L2_DV_BT_CEA_720X576I50,
	V4L2_DV_BT_CEA_1920X1080P50,
	V4L2_DV_BT_CEA_1920X1080P24,
	V4L2_DV_BT_CEA_1920X1080P25,
	V4L2_DV_BT_CEA_1920X1080P30,*/
	{ },
};

#endif /* _MX6_DISPLAY_H */
