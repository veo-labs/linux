/*
 * Video Display driver for Freescale i.MX6 SOC
 *
 * Copyright (c) 2015 Vodalys-Labs.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/pinctrl/consumer.h>
#include <linux/platform_device.h>
#include <linux/platform_data/camera-mx6.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <media/imx6.h>
#include <media/media-device.h>
#include <linux/interrupt.h>
#include <media/videobuf2-dma-contig.h>
#include <media/v4l2-device.h>
#include <media/v4l2-ioctl.h>
#include <media/v4l2-ctrls.h>
#include <media/v4l2-event.h>
#include <video/imx-ipu-v3.h>
#include <linux/v4l2-dv-timings.h>

#include "mx6-display.h"
#include "mx6-hdmi.h"

#define DEVICE_NAME "mx6-display"
#define DISPLAY_CONTROLS_NUMBER 1

/*
 * Min/Max supported width and heights.
 */
#define MIN_W       176U
#define MIN_H       144U
#define MAX_W      4096U
#define MAX_H      4096U

static const struct v4l2_dv_timings default_mode = V4L2_DV_BT_CEA_1280X720P50;

static const struct v4l2_dv_timings_cap mx6dis_dv_timings_cap = {
	.type = V4L2_DV_BT_656_1120,
	/* keep this initialization for compatibility with GCC < 4.4.6 */
	.reserved = { 0 },
	V4L2_INIT_BT_TIMINGS(MIN_W, MAX_W, MIN_H, MAX_H, 25000000, 74250000,
			V4L2_DV_BT_STD_CEA861,
			V4L2_DV_BT_CAP_PROGRESSIVE | V4L2_DV_BT_CAP_INTERLACED)
};

static int of_dev_node_match(struct device *dev, void *data)
{
	return dev->of_node == data;
}

/**
 * mx6dis_v4l2timings_to_videomode - fill in @vm using @dv_timings,
 * @dv_timings: v4l2_dv_timings structure to use as source
 * @vm: videomode structure to use as destination
 *
 * Fills out @vm using the timings specified in @dv_timings.
 */
void mx6dis_v4l2timings_to_videomode(const struct v4l2_dv_timings *dv_timings,
				   struct videomode *vm)
{
	vm->hactive = dv_timings->bt.width;
	vm->hfront_porch = dv_timings->bt.hfrontporch;
	vm->hsync_len = dv_timings->bt.hsync;
	vm->hback_porch = dv_timings->bt.hbackporch;

	vm->vactive = dv_timings->bt.height;
	vm->vfront_porch = dv_timings->bt.vfrontporch;
	vm->vsync_len = dv_timings->bt.vsync;
	vm->vback_porch = dv_timings->bt.vbackporch;

	vm->pixelclock = dv_timings->bt.pixelclock;

	vm->flags = 0;
	if (dv_timings->bt.polarities & V4L2_DV_HSYNC_POS_POL)
		vm->flags |= DISPLAY_FLAGS_HSYNC_HIGH;
	else
		vm->flags |= DISPLAY_FLAGS_HSYNC_LOW;

	if (dv_timings->bt.polarities & V4L2_DV_VSYNC_POS_POL)
		vm->flags |= DISPLAY_FLAGS_VSYNC_HIGH;
	else
		vm->flags |= DISPLAY_FLAGS_VSYNC_LOW;

	if (dv_timings->bt.interlaced)
		vm->flags |= DISPLAY_FLAGS_INTERLACED;
}
EXPORT_SYMBOL_GPL(mx6dis_v4l2timings_to_videomode);

/*
 * Calculate banchwidth for given width height and vref
 */
static inline int calc_bandwidth(int width, int height, unsigned int vref)
{
	return width * height * vref;
}

/* Calculate vref for a given timing */
static int calc_vref(struct v4l2_dv_timings *timings)
{
	unsigned long htotal, vtotal;

	htotal = timings->bt.width + timings->bt.hbackporch +
			timings->bt.hfrontporch + timings->bt.hsync;
	vtotal = timings->bt.height + timings->bt.vbackporch +
			timings->bt.vfrontporch + timings->bt.vsync;

	return DIV_ROUND_UP_ULL(timings->bt.pixelclock, vtotal * htotal);
}

/*
 * Get mx6dis pixel format from fourcc code. Return -EINVAL if the format is
 * not supported by our device
 */
static struct mx6dis_pixfmt *mx6dis_get_format_by_fourcc(u32 fourcc)
{
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(mx6dis_pixformats) ; i++) {
		struct mx6dis_pixfmt *format = &mx6dis_pixformats[i];

		if (format->fourcc == fourcc)
			return format;
	}

	return ERR_PTR(-EINVAL);
}

/**
 * Update pixel format from timings parameter. Pixel format supported is
 * RGB3.
 */
static int update_format_from_timings(struct v4l2_pix_format *pix_format,
				    struct v4l2_dv_timings *timings)
{
	int ret = 0;

	pix_format->width = timings->bt.width;
	pix_format->height = timings->bt.height;
	if (timings->bt.interlaced) {
		pix_format->field = V4L2_FIELD_ALTERNATE;
		pix_format->height /= 2;
	} else {
		pix_format->field = V4L2_FIELD_NONE;
	}

	/*
	 * Only RGB24 format supported, bytesperline is width * 3.
	 */
	pix_format->pixelformat = V4L2_PIX_FMT_RGB24;
	pix_format->bytesperline = pix_format->width * 3;
	pix_format->sizeimage = pix_format->bytesperline * pix_format->height;
	pix_format->priv = 0;
	pix_format->colorspace = V4L2_COLORSPACE_REC709;

	return ret;
}

/*
 * Get the container mx6dis_buffer structure from vb2_buffer
 */
static inline struct mx6dis_buffer *to_mx6dis_vb(struct vb2_buffer *vb)
{
	return container_of(vb, struct mx6dis_buffer, vb);
}

/*
 * Get the container mx6dis_ctx structure from file
 */
static inline struct mx6dis_ctx *file2ctx(struct file *file)
{
	return container_of(file->private_data, struct mx6dis_ctx, fh);
}

/*
 * Search into device_node the value of ipu
 */
static struct ipu_soc *mx6dis_find_ipu(struct mx6dis_dev *dev)
{
	struct device_node *node = dev->dev->of_node;
	struct device_node *ipu_node;
	struct device *ipu_dev;

	ipu_node = of_parse_phandle(node, "ipu", 0);
	if (!ipu_node) {
		dev_err(dev->dev, "missing ipu phandle!\n");
		return NULL;
	}

	dev_dbg(dev->dev, "IPU node found\n");

	ipu_dev = bus_find_device(&platform_bus_type, NULL,
				  ipu_node, of_dev_node_match);
	of_node_put(ipu_node);
	if (!ipu_dev) {
		dev_err(dev->dev, "failed to find ipu device!\n");
		return NULL;
	}

	dev_dbg(dev->dev, "IPU dev found\n");

	return dev_get_drvdata(ipu_dev);
}

/*
 * Release IPU resources
 */
static int mx6dis_put_ipu_resources(struct mx6dis_dev *dev)
{
	struct mx6dis_ctx *ctx = dev->ctx;

	if (!dev->ctx)
		return -EINVAL;

	if (!IS_ERR_OR_NULL(ctx->dis_ch))
		ipu_idmac_put(ctx->dis_ch);
	ctx->dis_ch = NULL;

	if (!IS_ERR_OR_NULL(ctx->dmfc))
		ipu_dmfc_put(ctx->dmfc);
	ctx->dmfc = NULL;

	if (!IS_ERR_OR_NULL(ctx->di))
		ipu_di_put(ctx->di);
	ctx->di = NULL;

	if (!IS_ERR_OR_NULL(ctx->dc))
			ipu_dc_put(ctx->dc);
	ctx->dc = NULL;

	if (!IS_ERR_OR_NULL(ctx->dp))
				ipu_dp_put(ctx->dp);
	ctx->dp = NULL;

	return 0;
}

/*
 * Get IPU resources for the current configuration
 */
static int mx6dis_get_ipu_resources(struct mx6dis_dev *dev)
{
	struct mx6dis_ctx *ctx = dev->ctx;
	int di_id, display_ch_num, err;

	if (!dev->ctx) {
		dev_err(dev->dev, "Wrong context: device open?\n");
		return -EINVAL;
	}

	/*
	 * Get IPU channel MEM-->DMFC-->DI channel corresponding
	 * to the IPU and DI IDs (channel 23)
	 */
	display_ch_num = IPUV3_CHANNEL_MEM_BG_SYNC;
	ctx->dis_ch = ipu_idmac_get(dev->ipu, display_ch_num);
	if (IS_ERR(ctx->dis_ch)) {
		dev_err(dev->dev, "could not get IDMAC channel %u\n",
				display_ch_num);
		err = PTR_ERR(ctx->dis_ch);
		goto out;
	}

	dev_dbg(dev->dev, "Direct MEM -> DMFC -> DI\n");

	/*
	 * Get DMFC for IDMAC channel
	 */
	ctx->dmfc = ipu_dmfc_get(dev->ipu, display_ch_num);
	if (IS_ERR(ctx->dmfc)) {
		dev_err(dev->dev, "failed to get DMFC\n");
		err = PTR_ERR(ctx->dmfc);
		goto out;
	}

	/*
	 * Get Display Processor
	 */
	ctx->dp = ipu_dp_get(dev->ipu, 0);
	if (IS_ERR(ctx->dp)) {
		err = PTR_ERR(ctx->dp);
		dev_err(dev->dev, "failed to get dp 0: %d\n", err);
		goto out;
	}

	/* Get Display Control channel 5 for Channel 23.
	 * i.MX6 Quad Applications Processor Reference Manual, Rev. 2
	 * Table 37-23. Display port channels */
	ctx->dc = ipu_dc_get(dev->ipu, 5);
	if (IS_ERR(ctx->dc)) {
		dev_err(dev->dev, "failed to get DC channel 5\n");
		err = PTR_ERR(ctx->dc);
		goto out;
	}

	/*
	 * Get Display Interface module
	 */
	di_id = dev->di_id;
	ctx->di = ipu_di_get(dev->ipu, di_id);
	if (IS_ERR(ctx->di)) {
		dev_err(dev->dev, "failed to get DI %d with error %ld\n",
				di_id,
				PTR_ERR(ctx->di));
		err = PTR_ERR(ctx->di);
		goto out;
	}

	return 0;
out:
	mx6dis_put_ipu_resources(dev);
	return err;
}

/*
 * In case of buf has been allocated, this function will free it
 */
static void mx6dis_free_dma_buf(struct mx6dis_dev *dev,
				 struct mx6dis_dma_buf *buf)
{
	if (buf->virt)
		dma_free_coherent(dev->dev, buf->len, buf->virt, buf->phys);

	buf->virt = NULL;
	buf->phys = 0;
}

/*
 * Alloc a MX6 Display dma buffer
 */
static int mx6dis_alloc_dma_buf(struct mx6dis_dev *dev,
				 struct mx6dis_dma_buf *buf,
				 int size)
{
	mx6dis_free_dma_buf(dev, buf);

	buf->len = PAGE_ALIGN(size);
	buf->virt = dma_alloc_coherent(dev->dev, buf->len, &buf->phys,
				       GFP_DMA | GFP_KERNEL);
	if (!buf->virt) {
		dev_err(dev->dev, "failed to alloc dma buffer\n");
		return -ENOMEM;
	}

	return 0;
}

/*
 * Setup IPU channel with the current format and timings parameters
 */
static int mx6dis_setup_channel(struct mx6dis_dev *dev,
				  struct ipuv3_channel *channel,
				  dma_addr_t addr0, dma_addr_t addr1)
{
	struct mx6dis_ctx *ctx = dev->ctx;
	u32 width, height, stride;
	struct ipu_image image;
	int err;

	width = ctx->out_fmt.width;
	height = ctx->out_fmt.height;

	/* Calculate the stride */
	stride = width * dev->display_pixfmt->ybpp;

	ipu_cpmem_zero(channel);

	memset(&image, 0, sizeof(image));
	image.pix.width = image.rect.width = width;
	image.pix.height = image.rect.height = height;
	image.pix.bytesperline = stride;
	image.pix.pixelformat = ctx->out_fmt.pixelformat;
	image.phys0 = addr0;
	image.phys1 = addr1;

	/* Setup DisplayProcessor channel for doing no conversion */
	err = ipu_dp_setup_channel(ctx->dp, ctx->in_cs,	ctx->out_cs);
	if (err) {
		dev_err(dev->dev,
			"initializing display processor failed with %d\n",
			err);
		return err;
	}

	err = ipu_dmfc_init_channel(ctx->dmfc, image.pix.width);
	if (err) {
		dev_err(dev->dev,
				"initializing dmfc channel failed with %d\n",
				err);
		return err;
	}

	/* Use 64 as burtsize*/
	err = ipu_dmfc_alloc_bandwidth(ctx->dmfc,
					calc_bandwidth(image.pix.width,
					image.pix.height,
					calc_vref(&dev->mx6dis_timings)), 64);
	if (err) {
		dev_err(dev->dev,
			"allocating dmfc bandwidth failed with %d\n", err);
		return err;
	}

	/* Configure CPMEM buffers */
	ipu_cpmem_zero(ctx->dis_ch);
	err = ipu_cpmem_set_image(ctx->dis_ch, &image);
	if (err) {
		dev_err(dev->dev,
				"fail configuring CPMEM parameters %d\n", err);
		return err;
	}

	ipu_cpmem_set_high_priority(ctx->dis_ch);
	ipu_idmac_set_double_buffer(ctx->dis_ch, true);

	return 0;
}

/*
 * Just verify if the stream has finished
 */
static irqreturn_t mx6dis_eof_interrupt(int irq, void *dev_id)
{
	struct mx6dis_ctx *ctx = dev_id;
	struct mx6dis_dev *dev = ctx->dev;
	unsigned long flags;

	spin_lock_irqsave(&dev->irqlock, flags);

	if (ctx->last_eof) {
		complete(&ctx->last_eof_comp);
		ctx->active_frame[ctx->buf_num] = NULL;
		ctx->last_eof = false;
		dev_dbg(dev->dev,
			   "EOF Interrupt detected last_eof\n");
		goto next;
	}

	/* bump the EOF timeout timer */
	mod_timer(&ctx->eof_timeout_timer,
		  jiffies + msecs_to_jiffies(MX6DIS_EOF_TIMEOUT));

next:
	spin_unlock_irqrestore(&dev->irqlock, flags);

	return IRQ_HANDLED;
}

static irqreturn_t mx6dis_nfack_interrupt(int irq, void *dev_id)
{
	struct mx6dis_ctx *ctx = dev_id;
	struct mx6dis_dev *dev = ctx->dev;
	struct mx6dis_buffer *frame;
	unsigned long flags;
	dma_addr_t phys;

	spin_lock_irqsave(&dev->irqlock, flags);
	dev_dbg(dev->dev, "NFACK display %d\n", ctx->buf_num);
	frame = ctx->active_frame[ctx->buf_num];
	if (!frame) {
		dev_dbg(dev->dev,
				"There is no frame to dequeue, setting next\n");
		if (!list_empty(&dev->buf_list)) {
			frame = list_entry(dev->buf_list.next,
					struct mx6dis_buffer, list);
			phys = vb2_dma_contig_plane_dma_addr(&frame->vb, 0);
			list_del(&frame->list);
			ctx->active_frame[ctx->buf_num] = frame;
			vb2_buffer_done(&frame->vb, VB2_BUF_STATE_DONE);
		} else {
			dev_dbg(dev->dev,
				"Next buffer will be dropped\n");
			phys = ctx->underrun_buf.phys;
		}

		/* Configure IPU buffers for the new physical address */
		ipu_cpmem_set_buffer(ctx->dis_ch, ctx->buf_num, phys);
		ipu_idmac_select_buffer(ctx->dis_ch, ctx->buf_num);

	} else {
		dev_dbg(dev->dev,
			"active_frame %d: Frame %d is being used by user\n",
			ctx->buf_num,
			frame->vb.v4l2_buf.index);
	}

	ctx->buf_num ^= 1;
	spin_unlock_irqrestore(&dev->irqlock, flags);

	return IRQ_HANDLED;
}

static int mx6dis_synchronize_interface(struct mx6dis_dev *dev)
{
	int ret = 0;
	struct ipu_di_signal_cfg sig_cfg = {};

	if (dev->stream_on)
		return ret;

	/* Configure signals for DI */
	sig_cfg.enable_pol = 1;
	sig_cfg.clk_pol = 0;
	sig_cfg.pixel_fmt = dev->ctx->out_fmt.pixelformat;
	sig_cfg.v_to_h_sync = 0;
	sig_cfg.hsync_pin = 2; /* Taken from imx_drm_core */
	sig_cfg.vsync_pin = 3;
	sig_cfg.clkflags = 0; /* HDMI */

	mx6dis_v4l2timings_to_videomode(&dev->mx6dis_timings, &sig_cfg.mode);

	/* Synchronize IPU with HDMI Transmitter */
	ret = ipu_dc_init_sync(dev->ctx->dc, dev->ctx->di,
				dev->mx6dis_timings.bt.interlaced,
				dev->ctx->out_fmt.pixelformat,
				dev->ctx->out_fmt.width);
	if (ret) {
		dev_err(dev->dev,
			"initializing display controller failed with %d\n",
			ret);
		return ret;
	}

	dev_dbg(dev->dev, "DC synchronized\n");

	ret = ipu_di_init_sync_panel(dev->ctx->di, &sig_cfg);
	if (ret) {
		dev_err(dev->dev,
				"initializing panel failed with %d\n", ret);
		return ret;
	}
	dev_dbg(dev->dev, "DI synchronized\n");

	return ret;
}

/*
 * Start/Stop streaming.
 */
static int start_display(struct mx6dis_dev *dev)
{
	struct mx6dis_ctx *ctx = dev->ctx;
	struct mx6dis_buffer *frame, *tmp;
	dma_addr_t phys[2] = {0};
	int err, i = 0;

	err = mx6dis_get_ipu_resources(dev);
	if (err) {
		dev_err(dev->dev, "Error taking ipu resources\n");
		return err;
	}

	/* Send subdev s_stream command */
	if (dev->mx6dis_hdmi)
		err = v4l2_subdev_call(&dev->mx6dis_hdmi->sd,
					video, s_stream, true);

	/* Init context formats */
	ctx->in_fmt = dev->output_fmt;
	ctx->in_cs = ipu_pixelformat_to_colorspace(ctx->in_fmt.pixelformat);
	ctx->out_fmt = dev->output_fmt;
	ctx->out_cs = ipu_pixelformat_to_colorspace(ctx->out_fmt.pixelformat);
	ctx->buf_num = 0;

	/*
	 * FIXME This driver version does not accept Color Conversion (using CP)
	 * That's why for the moment in_fmt and out_fmt should be equals
	 */
	if (ctx->in_cs != ctx->out_cs)
		return -EINVAL;

	/* Prepare two buffers for start streaming */
	list_for_each_entry_safe(frame, tmp, &dev->buf_list, list) {
		phys[i] = vb2_dma_contig_plane_dma_addr(&frame->vb, 0);
		list_del(&frame->list);
		vb2_buffer_done(&frame->vb, VB2_BUF_STATE_DONE);
		dev_dbg(dev->dev, "buffer %d done\n",
			 frame->vb.v4l2_buf.index);
		ctx->active_frame[i++] = frame;
		if (i >= 2)
			break;
	}

	/* Configure underrun buffer */
	err = mx6dis_alloc_dma_buf(dev, &ctx->underrun_buf,
				    ctx->out_fmt.sizeimage);
	if (err) {
		dev_err(dev->dev, "failed to alloc underrun_buf, %d\n",
				err);
		goto out_put_ipu;
	}

	/* init HDMI Display */
	err = mx6dis_synchronize_interface(dev);
	if (err) {
		dev_err(dev->dev, "Failed configuring HDMI interface\n");
		goto out_free_buf;
	}

	/* init the display MEM-->DI IDMAC channel */
	err = mx6dis_setup_channel(dev, ctx->dis_ch, phys[0], phys[1]);
	if (err) {
		dev_err(dev->dev, "failed configuring IPU channels\n");
		goto out_free_buf;
	}

	/* Enable display modules */
	ipu_dc_enable(dev->ipu);
	ipu_dp_enable(dev->ipu);

	/* Enable dmfc */
	err = ipu_dmfc_enable_channel(ctx->dmfc);
	if (err) {
		dev_err(dev->dev, "failed enabling DMFC channel\n");
		goto out_free_buf;
	}

	/* enable the idmac channel*/
	err = ipu_idmac_enable_channel(ctx->dis_ch);
	if (err) {
		dev_err(dev->dev, "failed enabling IDMAC channel\n");
		goto out_free_buf;
	}

	/* Enable Display Processor Channel */
	err = ipu_dp_enable_channel(ctx->dp);
	if (err) {
		dev_err(dev->dev,
				"failed enabling Display Processor channel\n");
		goto out_free_buf;
	}

	/* Start DC channel and DI after IDMAC */
	ipu_dc_enable_channel(ctx->dc);
	err = ipu_di_enable(ctx->di);
	if (err) {
		dev_err(dev->dev, "DI enable error: %d\n", err);
		goto out_free_buf;
	}

	/* set buffers ready */
	ipu_idmac_select_buffer(ctx->dis_ch, 0);
	ipu_idmac_select_buffer(ctx->dis_ch, 1);

	/* Configure EOF irq */
	ctx->eof_irq = ipu_idmac_channel_irq(
				dev->ipu, ctx->dis_ch, IPU_IRQ_EOF);

	err = devm_request_irq(dev->dev, ctx->eof_irq,
			mx6dis_eof_interrupt, 0,
			"mx6dis-eof", ctx);
	if (err) {
		dev_err(dev->dev,
				"Error registering display eof irq: %d\n", err);
		goto out_put_ipu;
	}

	/* Configure NFACK irq */
	ctx->nfack_irq = ipu_idmac_channel_irq(
				dev->ipu, ctx->dis_ch, IPU_IRQ_NFACK);

	err = devm_request_irq(dev->dev, ctx->nfack_irq,
			mx6dis_nfack_interrupt, 0,
			"mx6dis-nfack", ctx);
	if (err) {
		dev_err(dev->dev,
				"Error registering display nfack irq: %d\n",
				err);
		goto out_free_eof_irq;
	}

	/* start the EOF timeout timer */
	mod_timer(&ctx->eof_timeout_timer,
		  jiffies + msecs_to_jiffies(MX6DIS_EOF_TIMEOUT));

	/* Show cpmem info */
	ipu_cpmem_dump(ctx->dis_ch);

	return 0;

out_free_eof_irq:
	devm_free_irq(dev->dev, ctx->eof_irq, ctx);
out_free_buf:
	mx6dis_free_dma_buf(dev, &ctx->underrun_buf);
out_put_ipu:
	mx6dis_put_ipu_resources(dev);
	return err;
}

/*
 * EOF timeout timer function.
 */
static void mx6dis_eof_timeout(unsigned long data)
{
	struct mx6dis_dev *dev = (struct mx6dis_dev *)data;

	dev_dbg(dev->dev, "Display EOF timeout\n");
}

static int mx6dis_s_ctrl(struct v4l2_ctrl *ctrl)
{
	struct mx6dis_dev *dev = container_of(ctrl->handler,
					      struct mx6dis_dev, ctrl_hdlr);

	switch (ctrl->id) {
	case V4L2_CID_MIN_BUFFERS_FOR_OUTPUT:
		dev_dbg(dev->dev,
			"Setting min buffers control value to %d\n", ctrl->val);

		break;
	default:
		dev_err(dev->dev, "Invalid control\n");
		return -ERANGE;
	}

	return true;
}

static const struct v4l2_ctrl_ops mx6dis_ctrl_ops = {
	.s_ctrl = mx6dis_s_ctrl,
};

/*
 * Initialize device controls. Only V4L2_CID_MIN_BUFFERS_FOR_OUTPUT right now
 */
static int mx6dis_init_controls(struct mx6dis_dev *dev)
{
	struct v4l2_ctrl_handler *hdlr = &dev->ctrl_hdlr;
	int ret;

	v4l2_ctrl_handler_init(hdlr, DISPLAY_CONTROLS_NUMBER);

	v4l2_ctrl_new_std(hdlr, &mx6dis_ctrl_ops,
			  V4L2_CID_MIN_BUFFERS_FOR_OUTPUT, 2, 32, 1, 2);

	if (hdlr->error) {
		ret = hdlr->error;
		v4l2_ctrl_handler_free(hdlr);
		return ret;
	}

	dev->v4l2_dev.ctrl_handler = hdlr;
	dev->vfd.ctrl_handler = hdlr;

	v4l2_ctrl_handler_setup(hdlr);

	return 0;
}

/*
 * Video4linux2 ioctls
 */
static int videoc_querycap(struct file *file, void *priv,
			   struct v4l2_capability *cap)
{
	strlcpy(cap->driver, DEVICE_NAME, sizeof(cap->driver));
	strlcpy(cap->card, DEVICE_NAME, sizeof(cap->card));
	strlcpy(cap->bus_info, "platform:mx6-display", sizeof(cap->bus_info));
	cap->device_caps = V4L2_CAP_STREAMING | V4L2_CAP_VIDEO_OUTPUT;
	cap->capabilities = cap->device_caps | V4L2_CAP_DEVICE_CAPS;

	return 0;
}

/*
 * Different formats supported enumeration
 */
static int videoc_enum_fmt_vid_out(struct file *file, void  *priv,
					struct v4l2_fmtdesc *f)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;
	struct mx6dis_pixfmt *fmt;

	dev_dbg(dev->dev, "Enum video output formats\n");

	if (f->index >= NUM_FORMATS)
		return -EINVAL;

	fmt = &mx6dis_pixformats[f->index];
	strlcpy(f->description, fmt->name, sizeof(f->description));
	f->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	f->flags = 0;
	f->pixelformat = fmt->fourcc;

	return 0;
}

/*
 * Try the format and not change driver state
 */
static int videoc_try_fmt_vid_out(struct file *file, void *priv,
				  struct v4l2_format *f)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;
	const struct mx6dis_pixfmt *info;
	struct v4l2_pix_format *pix = &f->fmt.pix;

	dev_dbg(dev->dev,
			"%s: trying format %dx%d , %c%c%c%c\n",
			__func__,
			pix->width, pix->height,
			pix->pixelformat & 0xff,
			(pix->pixelformat >> 8) & 0xff,
			(pix->pixelformat >> 16) & 0xff,
			(pix->pixelformat >> 24) & 0xff);

	if (f->type != V4L2_BUF_TYPE_VIDEO_OUTPUT)
		return -EINVAL;

	/*
	 * Retrieve format information and select the default format if the
	 * requested format isn't supported.
	 */
	info = mx6dis_get_format_by_fourcc(pix->pixelformat);
	if (IS_ERR(info))
		info = mx6dis_get_format_by_fourcc(V4L2_PIX_FMT_RGB24);

	pix->pixelformat = V4L2_PIX_FMT_RGB24;
	pix->colorspace = V4L2_COLORSPACE_REC709;
	pix->field = V4L2_FIELD_NONE;
	pix->width = clamp(pix->width, MIN_W, MAX_W);
	pix->height = clamp(pix->height, MIN_H, MAX_H);
	pix->bytesperline = pix->width * info->ybpp;
	pix->sizeimage = (pix->width * pix->height * info->depth) / 8;

	dev_dbg(dev->dev,
			"Final tried format: %dx%d , %c%c%c%c\n",
			pix->width, pix->height,
			pix->pixelformat & 0xff,
			(pix->pixelformat >> 8) & 0xff,
			(pix->pixelformat >> 16) & 0xff,
			(pix->pixelformat >> 24) & 0xff);

	return 0;
}

/*
 * Get the current format configured for the device
 */
static int vidioc_g_fmt_vid_out(struct file *file, void *priv,
				struct v4l2_format *f)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	f->type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	f->fmt.pix = dev->output_fmt;

	dev_dbg(dev->dev,
			"Getting current video output format: %dx%d , %c%c%c%c\n",
			f->fmt.pix.width, f->fmt.pix.height,
			f->fmt.pix.pixelformat & 0xff,
			(f->fmt.pix.pixelformat >> 8) & 0xff,
			(f->fmt.pix.pixelformat >> 16) & 0xff,
			(f->fmt.pix.pixelformat >> 24) & 0xff);
	return 0;
}

/*
 * Set the user desired format for output, could be different than display
 * format which could have wrong behavior
 */
static int videoc_s_fmt_vid_out(struct file *file, void *priv,
				struct v4l2_format *format)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	dev_dbg(dev->dev, "Setting video output format\n");

	videoc_try_fmt_vid_out(file, priv, format);
	if (vb2_is_busy(&dev->buffer_queue)) {
		dev_err(dev->dev, "%s queue busy\n", __func__);
		return -EBUSY;
	}

	/*
	 *  FIXME As this driver version is not prepare to use Display Processor
	 * to perform changes on video, it sets same input and output formats
	 */
	dev->output_fmt = format->fmt.pix;
	dev->input_fmt = format->fmt.pix;
	dev->display_pixfmt = mx6dis_get_format_by_fourcc(
						dev->output_fmt.pixelformat);

	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_ENUMOUTPUT. Only HDMI output
 * for the moment
 */
static int vidioc_enum_output(struct file *file, void *priv,
			     struct v4l2_output *output)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	dev_dbg(dev->dev, "Enumerating video output\n");

	/* Check index */
	if (output->index >= 1)
		return -EINVAL;

	/* TODO Get properties from subdev output */
	strncpy(output->name, "HDMI", 4);
	output->type = V4L2_OUTPUT_TYPE_ANALOG;
	output->capabilities = V4L2_OUT_CAP_DV_TIMINGS;

	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_G_OUTPUT
 */
static int vidioc_g_output(struct file *file, void *fh, unsigned int *i)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	dev_dbg(dev->dev,
		 "Getting current video output index (0 default)\n");

	*i = dev->current_output;
	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_S_OUTPUT
 */
static int vidioc_s_output(struct file *file, void *priv, unsigned index)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	dev_dbg(dev->dev,
			 "Setting output to %d\n", index);

	if (index > 0)
		return -EINVAL;

	if (vb2_is_busy(&dev->buffer_queue))
			return -EBUSY;

	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_DQBUF
 */
static int vidioc_dqbuf(struct file *file, void *fh, struct v4l2_buffer *b)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	int ret = vb2_ioctl_dqbuf(file, fh, b);

	if (!ret)
		b->sequence = dev->sequence++;

	return ret;
}

/*
 * Callback function to manage ioctl VIDIOC_G_TIMINGS
 */
static int vidioc_g_dv_timings(struct file *file, void *priv_fh,
				    struct v4l2_dv_timings *timings)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	*timings = dev->mx6dis_timings;

	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_S_TIMINGS
 */
static int vidioc_s_dv_timings(struct file *file, void *priv_fh,
				    struct v4l2_dv_timings *timings)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;
	int ret = 0;

	if (!timings)
		return -EINVAL;

	/* If match with current timing return 0 */
	if (v4l2_match_dv_timings(&dev->mx6dis_timings, timings, 0)) {
			dev_dbg(dev->dev,
					"%s: no change\n", __func__);
		return 0;
	}

	/* Set subdev timings if present */
	if (!dev->mx6dis_hdmi) {
		v4l2_warn(&dev->v4l2_dev,
				"It seems there is not HDMI output device\n");
	} else {
		ret = v4l2_subdev_call(&dev->mx6dis_hdmi->sd,
			video, s_dv_timings, timings);
	}

	if (ret)
		v4l2_warn(&dev->v4l2_dev,
				"Error setting HDMI output timings\n");
	dev->mx6dis_timings = *timings;
	update_format_from_timings(&ctx->out_fmt, &dev->mx6dis_timings);

	return ret;

}

/*
 * Callback function to manage ioctl VIDIOC_ENUM_TIMINGS
 */
static int vidioc_enum_dv_timings(struct file *file, void *priv_fh,
				    struct v4l2_enum_dv_timings *timings)
{
	struct mx6dis_ctx *ctx = file2ctx(file);
	struct mx6dis_dev *dev = ctx->dev;

	if (timings->index >= ARRAY_SIZE(mx6dis_timings) - 1)
		return -EINVAL;

	dev_dbg(dev->dev, "Enum DV timings %d\n",
		 timings->index);

	memset(timings->reserved, 0, sizeof(timings->reserved));
	timings->timings = mx6dis_timings[timings->index];
	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_DV_TIMINGS_CAP
 */
static int vidioc_dv_timings_cap(struct file *file, void *fh,
					struct v4l2_dv_timings_cap *cap)
{
	cap->type = mx6dis_dv_timings_cap.type;
	cap->bt = mx6dis_dv_timings_cap.bt;

	return 0;
}

/*
 * Callback function to manage ioctl VIDIOC_SUBSCRIBE_EVENT
 */
static int vidioc_subscribe_event(struct v4l2_fh *fh,
				const struct v4l2_event_subscription *sub)
{
	switch (sub->type) {
	case V4L2_EVENT_CTRL:
		return v4l2_ctrl_subscribe_event(fh, sub);
	default:
		break;
	}
	return -EINVAL;
};

/*
 * File operations
 */
static int mx6dis_open(struct file *file)
{
	struct mx6dis_dev *dev = video_drvdata(file);
	struct mx6dis_ctx *ctx;
	int ret;

	dev_dbg(dev->dev, "Opening dev %s\n",
		 dev_name(dev->dev));

	if (mutex_lock_interruptible(&dev->mutex))
		return -ERESTARTSYS;

	ctx = kzalloc(sizeof(*ctx), GFP_KERNEL);
	if (!ctx) {
		ret = -ENOMEM;
		goto unlock;
	}

	v4l2_fh_init(&ctx->fh, video_devdata(file));
	ctx->dev = dev;
	dev->ctx = ctx;
	v4l2_fh_add(&ctx->fh);
	ctx->io_allowed = false;

	/* Configure context timers */
	init_timer(&ctx->eof_timeout_timer);
	ctx->eof_timeout_timer.data = (unsigned long)dev;
	ctx->eof_timeout_timer.function = mx6dis_eof_timeout;

	file->private_data = &ctx->fh;

	mutex_unlock(&dev->mutex);

	return 0;

unlock:
	mutex_unlock(&dev->mutex);
	return ret;
}

/*
 * Queue operations
 */

/*
 * Setup the constraints of the queue
 */
static int mx6dis_queue_setup(struct vb2_queue *vq,
			      const struct v4l2_format *fmt,
			      unsigned int *nbuffers, unsigned int *nplanes,
			      unsigned int sizes[], void *alloc_ctxs[])
{
	struct mx6dis_dev *dev = vb2_get_drv_priv(vq);
	struct v4l2_ctrl *min_buffer_ctrl;
	int min_buffer;

	dev_dbg(dev->dev,
		"Setup queue, asking for %d buffers\n",
		*nbuffers);

	if ((vq->type != V4L2_BUF_TYPE_VIDEO_OUTPUT) ||
		(fmt && fmt->fmt.pix.sizeimage < dev->input_fmt.sizeimage))
		return -EINVAL;

	/* Getting the min buffer from ctrl_handler */
	min_buffer_ctrl = v4l2_ctrl_find(&dev->ctrl_hdlr,
			V4L2_CID_MIN_BUFFERS_FOR_OUTPUT);
	min_buffer = min_buffer_ctrl->val;

	/*
	 * Check sufficient buffer numbers have been allocated
	 */
	if (vq->num_buffers + *nbuffers < min_buffer)
		*nbuffers = min_buffer - vq->num_buffers;

	*nplanes = 1;
	sizes[0] = fmt ? fmt->fmt.pix.sizeimage : dev->input_fmt.sizeimage;
	alloc_ctxs[0] = dev->alloc_ctx;

	dev_dbg(dev->dev,
			"get %d buffer(s) of size %d each.\n",
			*nbuffers, sizes[0]);
	return 0;
}

/*
 * Queue a buffer into the driver. For Output device that means that the buffer
 * contains the display content to show.
 */
static void mx6dis_buf_queue(struct vb2_buffer *vb)
{
	struct mx6dis_dev *dev = vb2_get_drv_priv(vb->vb2_queue);
	struct mx6dis_buffer *buf_queue = to_mx6dis_vb(vb);
	struct mx6dis_buffer *buf_current;
	unsigned long flags;
	int i = -1;
	bool found = false;

	spin_lock_irqsave(&dev->irqlock, flags);

	dev_dbg(dev->dev,
			"queue buf %d: Timestamp %ld, state %d\n",
			buf_queue->vb.v4l2_buf.index,
			buf_queue->vb.v4l2_buf.timestamp.tv_usec,
			vb->state);

	/* Search the buffer on active_frames */
	do {
		buf_current = dev->ctx->active_frame[++i];
		found = buf_current && (buf_current->vb.v4l2_buf.index ==
						buf_queue->vb.v4l2_buf.index);
	} while (!found && i < 1);

	if (!found) {
		dev_dbg(dev->dev,
			"Queuing empty buffer");
		goto end;
	}

	/* Update active frame */
	dev->ctx->active_frame[i] = NULL;
end:
	dev_dbg(dev->dev,
				"buffer %d added to list\n",
				vb->v4l2_buf.index);
	/* Add to the list of buffers */
	list_add_tail(&buf_queue->list, &dev->buf_list);
	spin_unlock_irqrestore(&dev->irqlock, flags);
}

/*
 * Prepare the buffer for being queue to the DMA engine
 */
static int mx6dis_buf_prepare(struct vb2_buffer *vb)
{
	struct mx6dis_dev *dev = vb2_get_drv_priv(vb->vb2_queue);
	unsigned long size = dev->input_fmt.sizeimage;

	if (vb2_plane_size(vb, 0) < size) {
		dev_err(dev->dev, "buffer too small (%lu < %lu)\n",
				vb2_plane_size(vb, 0), size);
		return -EINVAL;
	}

	vb->v4l2_buf.field = V4L2_FIELD_NONE;
	vb2_set_plane_payload(vb, 0, size);

	dev_dbg(dev->dev, "Buffer %d prepared\n",
				 vb->v4l2_buf.index);
	return 0;
}

static int mx6dis_start_streaming(struct vb2_queue *vq, unsigned int count)
{
	struct mx6dis_dev *dev = vb2_get_drv_priv(vq);
	struct mx6dis_buffer *frame;
	unsigned long flags;
	int ret;

	dev->sequence = 0;

	ret = start_display(dev);
	if (ret) {
		spin_lock_irqsave(&dev->irqlock, flags);
		/* release all active buffers */
		while (!list_empty(&dev->buf_list)) {
			frame = list_entry(dev->buf_list.next,
					struct mx6dis_buffer, list);
			list_del(&frame->list);
			vb2_buffer_done(&frame->vb, VB2_BUF_STATE_QUEUED);
			dev_dbg(dev->dev, "buffer %d done\n",
				 frame->vb.v4l2_buf.index);
		}
		spin_unlock_irqrestore(&dev->irqlock, flags);
		dev_err(dev->dev, "streaming was not able to start\n");

	} else {
		dev_dbg(dev->dev, "streaming started\n");
		dev->stream_on = true;
	}
	return ret;
}

static void mx6dis_stop_streaming(struct vb2_queue *vq)
{
	struct mx6dis_dev *dev = vb2_get_drv_priv(vq);
	struct mx6dis_buffer *buf, *nbuf, *frame;
	unsigned long flags;
	int err, i;

	dev_dbg(dev->dev, "Stoping streaming\n");

	if (!dev->stream_on)
		return;

	/* stop the EOF timeout timer */
	del_timer_sync(&dev->ctx->eof_timeout_timer);

	/*
	 * Mark next EOF interrupt as the last before encoder off,
	 * and then wait for interrupt handler to mark completion.
	 */
	init_completion(&dev->ctx->last_eof_comp);
	dev->ctx->last_eof = true;
	err = wait_for_completion_timeout(&dev->ctx->last_eof_comp,
				msecs_to_jiffies(MX6DIS_EOF_TIMEOUT));
	if (err == 0)
		v4l2_warn(&dev->v4l2_dev, "wait last encode EOF timeout\n");

	if (dev->ctx->eof_irq)
		devm_free_irq(dev->dev, dev->ctx->eof_irq, dev->ctx);

	if (dev->ctx->nfack_irq)
		devm_free_irq(dev->dev, dev->ctx->nfack_irq, dev->ctx);

	if (dev->ctx->di)
		ipu_di_disable(dev->ctx->di);

	if (dev->ctx->dc)
		ipu_dc_disable(dev->ipu);

	if (dev->ctx->dp)
		ipu_dp_disable(dev->ipu);

	if (dev->ctx->dis_ch)
		ipu_idmac_disable_channel(dev->ctx->dis_ch);

	if (dev->ctx->dmfc)
		ipu_dmfc_disable_channel(dev->ctx->dmfc);

	/* Free underrun buffer and IPU resources */
	mx6dis_free_dma_buf(dev, &dev->ctx->underrun_buf);
	mx6dis_put_ipu_resources(dev);

	/* return any remaining active frames with error */
	for (i = 0; i < 2; i++) {
		frame = dev->ctx->active_frame[i];
		if (frame && frame->vb.state == VB2_BUF_STATE_ACTIVE) {
			dev_err(dev->dev,
					"[Display stop]frame seq: %d\n",
					frame->vb.v4l2_buf.sequence);
			vb2_buffer_done(&frame->vb, VB2_BUF_STATE_ERROR);
			dev->ctx->active_frame[i] = NULL;
		}
	}

	spin_lock_irqsave(&dev->irqlock, flags);
	dev->stream_on = false;
	list_for_each_entry_safe(buf, nbuf, &dev->buf_list, list) {
		vb2_buffer_done(&buf->vb, VB2_BUF_STATE_ERROR);
		list_del(&buf->list);
		dev_dbg(dev->dev, "buffer %d done\n",
			 buf->vb.v4l2_buf.index);
	}
	spin_unlock_irqrestore(&dev->irqlock, flags);

}

/* TODO */
/* queue_setup and buf_queue are mandatory */
static struct vb2_ops mx6dis_qops = {
	.queue_setup	 = mx6dis_queue_setup,
	.buf_prepare	 = mx6dis_buf_prepare,
	.buf_queue	 = mx6dis_buf_queue,
	.wait_prepare	 = vb2_ops_wait_prepare,
	.wait_finish	 = vb2_ops_wait_finish,
	.start_streaming = mx6dis_start_streaming,
	.stop_streaming  = mx6dis_stop_streaming,
};

static const struct v4l2_ioctl_ops mx6dis_ioctl_ops = {
	.vidioc_querycap		= videoc_querycap,
	.vidioc_enum_fmt_vid_out	= videoc_enum_fmt_vid_out,
	.vidioc_g_fmt_vid_out		= vidioc_g_fmt_vid_out,
	.vidioc_try_fmt_vid_out		= videoc_try_fmt_vid_out,
	.vidioc_s_fmt_vid_out		= videoc_s_fmt_vid_out,

	.vidioc_enum_output		= vidioc_enum_output,
	.vidioc_g_output		= vidioc_g_output,
	.vidioc_s_output		= vidioc_s_output,

	.vidioc_reqbufs			= vb2_ioctl_reqbufs,
	.vidioc_create_bufs		= vb2_ioctl_create_bufs,
	.vidioc_prepare_buf		= vb2_ioctl_prepare_buf,
	.vidioc_querybuf		= vb2_ioctl_querybuf,
	.vidioc_qbuf			= vb2_ioctl_qbuf,
	.vidioc_dqbuf			= vidioc_dqbuf,
	.vidioc_streamon		= vb2_ioctl_streamon,
	.vidioc_streamoff		= vb2_ioctl_streamoff,

	.vidioc_enum_dv_timings		= vidioc_enum_dv_timings,
	.vidioc_s_dv_timings		= vidioc_s_dv_timings,
	.vidioc_g_dv_timings		= vidioc_g_dv_timings,

	.vidioc_dv_timings_cap		= vidioc_dv_timings_cap,
/*	.vidioc_overlay         = vidioc_overlay,
	.vidioc_log_status      = vidioc_log_status,
	.vidioc_s_edid          = vidioc_s_edid,
	.vidioc_g_edid          = vidioc_g_edid,
	.vidioc_query_dv_timings = vidioc_query_dv_timings,

	.vidioc_enum_framesizes         = vidioc_enum_framesizes,
	.vidioc_enum_frameintervals     = vidioc_enum_frameintervals,
	.vidioc_g_parm          = vidioc_g_parm,
	.vidioc_s_parm          = vidioc_s_parm,

	.vidioc_g_fbuf          = vidioc_g_fbuf,
	.vidioc_s_fbuf          = vidioc_s_fbuf,

	.vidioc_cropcap         = vidioc_cropcap,
	.vidioc_g_crop          = vidioc_g_crop,
	.vidioc_s_crop          = vidioc_s_crop,
#ifdef CONFIG_VIDEO_ADV_DEBUG
	.vidioc_g_register      = vidioc_g_register,
	.vidioc_s_register	= vidioc_s_register,
#endif*/
	.vidioc_subscribe_event = vidioc_subscribe_event,
	.vidioc_unsubscribe_event = v4l2_event_unsubscribe,
};


static const struct v4l2_file_operations mx6dis_fops = {
	.owner		= THIS_MODULE,
	.open		= mx6dis_open,
	.release	= vb2_fop_release,
	.unlocked_ioctl	= video_ioctl2,
	.poll		= vb2_fop_poll,
	.mmap		= vb2_fop_mmap,
};

/*
 * Handle notifications from the subdevs.
 */
static void mx6dis_subdev_notification(struct v4l2_subdev *sd,
				       unsigned int notification,
				       void *arg)
{
	struct mx6dis_dev *dev;

	if (sd == NULL)
		return;

	dev = container_of(sd->v4l2_dev, struct mx6dis_dev, v4l2_dev);


	switch (notification) {
	case MX6DIS_HDMI_HOTPLUG:
		if (dev) {
			v4l2_event_queue(&dev->vfd, (struct v4l2_event *)arg);
			dev_dbg(dev->dev, "HDMI cable plugin\n");
			break;
		}
	}
}

static int mx6dis_graph_notify_bound(struct v4l2_async_notifier *notifier,
				struct v4l2_subdev *subdev,
				struct v4l2_async_subdev *asd)
{
	struct mx6dis_dev *dev =
		container_of(notifier, struct mx6dis_dev, notifier);

	dev_dbg(dev->dev, "Bounding subdev %s\n",
		 subdev->name);

	dev->mx6dis_hdmi = v4l2_get_subdevdata(subdev);

	if (!dev->mx6dis_hdmi) {
		dev_err(dev->dev, "Wrong HDMI Subdev\n");
		return -EINVAL;
	}

	return 0;
}

static int mx6dis_add_hdmi_transmitter(struct mx6dis_dev *dev)
{
	int ret = 0;
	struct platform_device *pdev;
	struct device_node *hdmi_node;
	struct v4l2_async_subdev **subdevs = NULL;

	pdev = container_of(dev->dev, struct platform_device, dev);
	if (!pdev)
		return -EINVAL;

	hdmi_node = of_find_compatible_node(NULL, NULL, "fsl,imx6q-hdmi");
	if (!hdmi_node)
		return -EINVAL;
	dev_dbg(dev->dev, "HDMI node found\n");

	/* Register the subdevices notifier */
	subdevs = devm_kzalloc(dev->dev, sizeof(*subdevs),
				GFP_KERNEL);
	if (!subdevs) {
		ret = -EINVAL;
		goto fail;
	}

	/* Configure as asynchronous HDMI Subdev */
	dev->asd.match_type = V4L2_ASYNC_MATCH_OF;
	dev->asd.match.of.node = hdmi_node;
	subdevs[0] = &dev->asd;

	dev->notifier.subdevs = subdevs;
	dev->notifier.num_subdevs = 1;
	dev->notifier.bound = mx6dis_graph_notify_bound;

	ret = v4l2_async_notifier_register(&dev->v4l2_dev,
						&dev->notifier);
	if (ret < 0)
		dev_err(dev->dev, "notifier registration failed\n");

fail:
	of_node_put(hdmi_node);
	return ret;
}

/*
 *
 */
static int mx6dis_graph_parse_dt(struct mx6dis_dev *dev)
{
	struct platform_device *pdev;
	struct device_node *node = dev->dev->of_node;
	struct device_node *di = NULL;
	int ret;

	pdev = container_of(dev->dev, struct platform_device, dev);
	if (!pdev)
		return -EINVAL;

	/* get our IPU */
	dev->ipu = mx6dis_find_ipu(dev);
	if (IS_ERR_OR_NULL(dev->ipu)) {
		dev_err(dev->dev, "could not get ipu\n");
		ret = -ENODEV;
		goto fail;
	}
	dev_dbg(dev->dev, "IPU soc found\n");

	/* Find and register HDMI Transmitter subdev */
	ret = mx6dis_add_hdmi_transmitter(dev);
	if (ret) {
		dev_err(dev->dev, "Error configuring HDMI device\n");
		goto fail;
	}

	/* Configuring DisplayInterface DI */
	di = of_get_next_child(node, NULL);
	ret = of_property_read_u32(di, "reg", &dev->di_id);
	if (!ret)
		dev_dbg(dev->dev,
				"add DI %d\n", dev->di_id);
	else
		dev_err(dev->dev,
			"Could not find reg property : %d\n",
			ret);

	/* For decrement refcount on hdmi_node */
	of_node_put(di);
	ret = 0;

fail:
	return ret;
}


static int mx6dis_probe(struct platform_device *pdev)
{
	struct mx6dis_dev *dev;
	struct video_device *vfd;
	struct pinctrl *pinctrl;
	int ret;

	dev = devm_kzalloc(&pdev->dev, sizeof(*dev), GFP_KERNEL);
	if (!dev)
		return -ENOMEM;

	dev->dev = &pdev->dev;
	mutex_init(&dev->mutex);
	spin_lock_init(&dev->irqlock);

	ret = v4l2_device_register(&pdev->dev, &dev->v4l2_dev);
	if (ret < 0) {
		dev_err(dev->dev, "V4L2 device registration failed (%d)\n",
			ret);
		return ret;
	}

	dev_dbg(dev->dev, "Register v4l2 device\n");

	pdev->dev.coherent_dma_mask = DMA_BIT_MASK(32);

	/* Parse the device node and configure it */
	ret = mx6dis_graph_parse_dt(dev);
	if (ret)
		goto unreg_dev;

	dev_dbg(dev->dev, "Parse device tree\n");

	vfd = &dev->vfd;

	/* Create device node */
	snprintf(vfd->name, sizeof(vfd->name), "%s", dev->dev->of_node->name);
	vfd->fops = &mx6dis_fops;
	vfd->ioctl_ops = &mx6dis_ioctl_ops;
	vfd->release = video_device_release_empty;
	vfd->v4l2_dev = &dev->v4l2_dev;
	vfd->queue = &dev->buffer_queue;
	vfd->vfl_dir = VFL_DIR_TX;

	/*
	 * Provide a mutex to v4l2 core. It will be used to protect
	 * all fops and v4l2 ioctls.
	 */
	vfd->lock = &dev->mutex;
	dev->v4l2_dev.notify = mx6dis_subdev_notification;

	/* Get any pins needed */
	pinctrl = devm_pinctrl_get_select_default(&pdev->dev);

	/* setup some defaults */
	dev->current_output = 0;
	dev->stream_on = false;
	dev->mx6dis_timings = default_mode;
	update_format_from_timings(&dev->output_fmt, &dev->mx6dis_timings);

	/* Initialize the buffer queues */
	dev->alloc_ctx = vb2_dma_contig_init_ctx(dev->dev);
	if (IS_ERR(dev->alloc_ctx)) {
		dev_err(dev->dev, "Failed to alloc vb2 context\n");
		goto unreg_dev;
	}

	dev->buffer_queue.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
	dev->buffer_queue.io_modes = VB2_MMAP | VB2_DMABUF;
	dev->buffer_queue.lock = &dev->mutex;
	dev->buffer_queue.drv_priv = dev;
	dev->buffer_queue.buf_struct_size = sizeof(struct mx6dis_buffer);
	dev->buffer_queue.ops = &mx6dis_qops;
	dev->buffer_queue.mem_ops = &vb2_dma_contig_memops;
	dev->buffer_queue.min_buffers_needed = 2;
	dev->buffer_queue.timestamp_flags = V4L2_BUF_FLAG_TIMESTAMP_COPY |
			V4L2_BUF_FLAG_TSTAMP_SRC_EOF;

	ret = vb2_queue_init(&dev->buffer_queue);
	if (ret < 0) {
		dev_err(dev->dev, "Failed to init vb2 queue: %d\n", ret);
		goto cleanup_ctx;
	}

	INIT_LIST_HEAD(&dev->buf_list);

	/* init our controls */
	ret = mx6dis_init_controls(dev);
	if (ret) {
		dev_err(dev->dev, "init controls failed\n");
		goto cleanup_queue;
	}

	ret = video_register_device(vfd, VFL_TYPE_GRABBER, -1);
	if (ret) {
		dev_err(dev->dev, "Failed to register video device\n");
		goto cleanup_queue;
	}

	video_set_drvdata(vfd, dev);
	platform_set_drvdata(pdev, dev);

	v4l2_info(&dev->v4l2_dev,
		  "Device registered as %s, on ipu%d\n",
		  video_device_node_name(vfd), ipu_get_num(dev->ipu));
	return 0;

cleanup_queue:
	vb2_queue_release(&dev->buffer_queue);
cleanup_ctx:
	if (!IS_ERR_OR_NULL(dev->alloc_ctx))
		vb2_dma_contig_cleanup_ctx(dev->alloc_ctx);
unreg_dev:
	v4l2_device_unregister(&dev->v4l2_dev);
	devm_kfree(&pdev->dev, dev);
	return ret;
}

static int mx6dis_remove(struct platform_device *pdev)
{
	struct mx6dis_dev *dev =
		(struct mx6dis_dev *)platform_get_drvdata(pdev);

	v4l2_info(&dev->v4l2_dev, "Removing " DEVICE_NAME "\n");
	v4l2_ctrl_handler_free(&dev->ctrl_hdlr);

	if (!IS_ERR_OR_NULL(dev->alloc_ctx))
		vb2_dma_contig_cleanup_ctx(dev->alloc_ctx);

	v4l2_device_unregister(&dev->v4l2_dev);

	return 0;
}

static const struct of_device_id mx6dis_dt_ids[] = {
	{ .compatible = "fsl,imx6-v4l2-display" },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, mx6dis_dt_ids);

static struct platform_driver mx6cam_pdrv = {
	.probe		= mx6dis_probe,
	.remove		= mx6dis_remove,
	.driver		= {
		.name	= DEVICE_NAME,
		.owner	= THIS_MODULE,
		.of_match_table	= mx6dis_dt_ids,
	},
};

module_platform_driver(mx6cam_pdrv);

MODULE_DESCRIPTION("i.MX6 v4l2 display driver");
MODULE_AUTHOR("Pablo Anton <pablo.antond@veo-labs.com>");
MODULE_LICENSE("GPL");
