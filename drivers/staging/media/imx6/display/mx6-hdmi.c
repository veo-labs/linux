/* HDMI Transmitter driver for Freescale i.MX6 SOC
 *
 * Copyright (c) 2015 Vodalys-Labs.
 *
 * The code has been derived from gpu/drm/bridge/dw_hdmi.c
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/mfd/syscon.h>
#include <linux/mfd/syscon/imx6q-iomuxc-gpr.h>
#include <linux/regmap.h>
#include <linux/irq.h>
#include <linux/i2c.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/hdmi.h>
#include <linux/of_device.h>
#include <media/v4l2-dv-timings.h>
#include <linux/v4l2-dv-timings.h>
#include <linux/videodev2.h>
#include <media/v4l2-device.h>

#include "../../../../gpu/drm/bridge/dw_hdmi.h"

#include "mx6-hdmi.h"

/* Module parameter for configuring debug level */
static int debug = 0;
module_param(debug, int, 0644);
MODULE_PARM_DESC(debug,
		 "Debug level (default 0). Values from 0-2 are allowed");

#define DRIVER_NAME "mx6-hdmi"

static const struct v4l2_dv_timings default_mode = V4L2_DV_BT_CEA_1280X720P50;

static const struct mx6dis_hdmi_mpll_config imx_hdmi_mpll_cfg[] = {
	{
		45250000, {
			{ 0x01e0, 0x0000 },
			{ 0x21e1, 0x0000 },
			{ 0x41e2, 0x0000 }
		},
	}, {
		92500000, {
			{ 0x0140, 0x0005 },
			{ 0x2141, 0x0005 },
			{ 0x4142, 0x0005 },
	},
	}, {
		148500000, {
			{ 0x00a0, 0x000a },
			{ 0x20a1, 0x000a },
			{ 0x40a2, 0x000a },
		},
	}, {
		~0UL, {
			{ 0x00a0, 0x000a },
			{ 0x2001, 0x000f },
			{ 0x4002, 0x000f },
		},
	}
};

/* Array with possible pixel clo */
static const struct mx6dis_hdmi_curr_ctrl imx_hdmi_cur_ctr[] = {
	/*      pixelclk     bpp8    bpp10   bpp12 */
	{
		54000000, { 0x091c, 0x091c, 0x06dc },
	}, {
		58400000, { 0x091c, 0x06dc, 0x06dc },
	}, {
		72000000, { 0x06dc, 0x06dc, 0x091c },
	}, {
		74250000, { 0x06dc, 0x0b5c, 0x091c },
	}, {
		118800000, { 0x091c, 0x091c, 0x06dc },
	}, {
		216000000, { 0x06dc, 0x0b5c, 0x091c },
	}, {
		~0UL, { 0x0000, 0x0000, 0x0000 },
	},
};

static const struct mx6dis_hdmi_sym_term imx_hdmi_sym_term[] = {
	/*pixelclk   symbol   term*/
	{ 148500000, 0x800d, 0x0005 },
	{ ~0UL,      0x0000, 0x0000 }
};

/**
 * Update pixel format from timings parameter. Pixel format supported are
 * RGB3, YVYU and Y444. For other pixel format the bytesperline is not
 * calculated
 */
static int update_format_from_timings(struct v4l2_pix_format *pix_format,
				    struct v4l2_dv_timings *timings)
{
	int ret = 0;

	pix_format->width = timings->bt.width;
	pix_format->height = timings->bt.height;
	if (timings->bt.interlaced) {
		pix_format->field = V4L2_FIELD_ALTERNATE;
		pix_format->height /= 2;
	} else {
		pix_format->field = V4L2_FIELD_NONE;
	}

	/*
	 * The RGB24 format is four bytes for pixel, bytesperline is width * 3.
	 */
	if (pix_format->pixelformat == V4L2_PIX_FMT_RGB24)
		pix_format->bytesperline = pix_format->width * 3;
	else if (pix_format->pixelformat == V4L2_PIX_FMT_YVYU)
		pix_format->bytesperline = pix_format->width * 2;
	else if (pix_format->pixelformat == V4L2_PIX_FMT_YUV444)
		pix_format->bytesperline = pix_format->width * 3;
	else
		ret = -EINVAL;

	pix_format->sizeimage = pix_format->bytesperline * pix_format->height;
	pix_format->priv = 0;

	return ret;
}

static bool find_cea861e_index_timings(struct mx6dis_hdmi_state *hdmi,
				  struct v4l2_dv_timings *timings)
{
	bool found = false;
	int length = ARRAY_SIZE(cea_861e_timings);
	int i = 0;

	while (!found && i < length) {

		found = v4l2_match_dv_timings(timings,
				&cea_861e_timings[i], 0);
		i++;
	}

	if (found) {
		v4l2_dbg(1, debug, &hdmi->sd, "Found Timings index %d\n", i);
		hdmi->vic = i;
	}
	return  found;
}

static struct mx6dis_hdmi_plat_data imx6q_hdmi_drv_data = {
	.mpll_cfg   = imx_hdmi_mpll_cfg,
	.cur_ctr    = imx_hdmi_cur_ctr,
	.sym_term   = imx_hdmi_sym_term,
};

static void mx6dis_hdmi_writeb(struct mx6dis_hdmi_state *hdmi, u8 val,
			       int offset)
{
	writeb(val, hdmi->regs + offset);
}

static u8 mx6dis_hdmi_readb(struct mx6dis_hdmi_state *hdmi, int offset)
{
	return readb(hdmi->regs + offset);
}

static inline void hdmi_writeb(struct mx6dis_hdmi_state *hdmi,
			       u8 val, int offset)
{
	hdmi->write(hdmi, val, offset);
}

static inline u8 hdmi_readb(struct mx6dis_hdmi_state *hdmi, int offset)
{
	return hdmi->read(hdmi, offset);
}

static void hdmi_modb(struct mx6dis_hdmi_state *hdmi,
		      u8 data, u8 mask, unsigned reg)
{
	u8 val = hdmi_readb(hdmi, reg) & ~mask;

	val |= data & mask;
	hdmi_writeb(hdmi, val, reg);
}

static void hdmi_mask_writeb(struct mx6dis_hdmi_state *hdmi,
			     u8 data, unsigned int reg, u8 shift, u8 mask)
{
	hdmi_modb(hdmi, data << shift, mask, reg);
}

static void mx6dis_hdmi_phy_sel_data_en_pol(struct mx6dis_hdmi_state *hdmi,
					    u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_SELDATAENPOL_OFFSET,
			 HDMI_PHY_CONF0_SELDATAENPOL_MASK);
}

static void mx6dis_hdmi_phy_sel_interface_control(
		struct mx6dis_hdmi_state *hdmi, u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_SELDIPIF_OFFSET,
			 HDMI_PHY_CONF0_SELDIPIF_MASK);
}

static void mx6dis_hdmi_phy_enable_tmds(struct mx6dis_hdmi_state *hdmi,
					u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_ENTMDS_OFFSET,
			 HDMI_PHY_CONF0_ENTMDS_MASK);
}

static void mx6dis_hdmi_phy_enable_power(struct mx6dis_hdmi_state *hdmi,
					 u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_PDZ_OFFSET,
			 HDMI_PHY_CONF0_PDZ_MASK);
}

static void mx6dis_hdmi_phy_gen2_txpwron(struct mx6dis_hdmi_state *hdmi,
					 u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_GEN2_TXPWRON_OFFSET,
			 HDMI_PHY_CONF0_GEN2_TXPWRON_MASK);
}

static void mx6dis_hdmi_phy_gen2_pddq(struct mx6dis_hdmi_state *hdmi, u8 enable)
{
	hdmi_mask_writeb(hdmi, enable, HDMI_PHY_CONF0,
			 HDMI_PHY_CONF0_GEN2_PDDQ_OFFSET,
			 HDMI_PHY_CONF0_GEN2_PDDQ_MASK);
}

static inline void mx6dis_hdmi_phy_test_clear(struct mx6dis_hdmi_state *hdmi,
				       unsigned char bit)
{
	hdmi_modb(hdmi, bit << HDMI_PHY_TST0_TSTCLR_OFFSET,
		  HDMI_PHY_TST0_TSTCLR_MASK, HDMI_PHY_TST0);
}

static bool hdmi_phy_wait_i2c_done(struct mx6dis_hdmi_state *hdmi, int msec)
{
	u32 val;

	while ((val = hdmi_readb(hdmi, HDMI_IH_I2CMPHY_STAT0) & 0x3) == 0) {
		if (msec-- == 0)
			return false;
		udelay(1000);
	}
	hdmi_writeb(hdmi, val, HDMI_IH_I2CMPHY_STAT0);

	return true;
}

static void __mx6dis_hdmi_phy_i2c_write(struct mx6dis_hdmi_state *hdmi,
					unsigned short data,
					unsigned char addr)
{
	hdmi_writeb(hdmi, 0xFF, HDMI_IH_I2CMPHY_STAT0);
	hdmi_writeb(hdmi, addr, HDMI_PHY_I2CM_ADDRESS_ADDR);
	hdmi_writeb(hdmi, (unsigned char)(data >> 8),
		    HDMI_PHY_I2CM_DATAO_1_ADDR);
	hdmi_writeb(hdmi, (unsigned char)(data >> 0),
		    HDMI_PHY_I2CM_DATAO_0_ADDR);
	hdmi_writeb(hdmi, HDMI_PHY_I2CM_OPERATION_ADDR_WRITE,
		    HDMI_PHY_I2CM_OPERATION_ADDR);
	hdmi_phy_wait_i2c_done(hdmi, 1000);
}

static int mx6dis_hdmi_phy_i2c_write(struct mx6dis_hdmi_state *hdmi,
				     unsigned short data,
				     unsigned char addr)
{
	__mx6dis_hdmi_phy_i2c_write(hdmi, data, addr);
	return 0;
}

static int mx6dis_hdmi_phy_configure(struct mx6dis_hdmi_state *hdmi,
				     unsigned char prep,
				     unsigned char res,
				     int cscon)
{
	unsigned res_idx, i;
	u8 val, msec;
	const struct mx6dis_hdmi_mpll_config *mpll_config =
						hdmi->plat_data->mpll_cfg;
	const struct mx6dis_hdmi_curr_ctrl *curr_ctrl =
			hdmi->plat_data->cur_ctr;
	const struct mx6dis_hdmi_sym_term *sym_term =
			hdmi->plat_data->sym_term;

	if (prep)
		return -EINVAL;

	switch (res) {
	case 0:	/* color resolution 0 is 8 bit color depth */
	case 8:
		res_idx = IMX6_HDMI_RES_8;
		break;
	case 10:
		res_idx = IMX6_HDMI_RES_10;
		break;
	case 12:
		res_idx = IMX6_HDMI_RES_12;
		break;
	default:
		return -EINVAL;
	}

	/* Enable csc path */
	if (cscon)
		val = HDMI_MC_FLOWCTRL_FEED_THROUGH_OFF_CSC_IN_PATH;
	else
		val = HDMI_MC_FLOWCTRL_FEED_THROUGH_OFF_CSC_BYPASS;

	hdmi_writeb(hdmi, val, HDMI_MC_FLOWCTRL);

	/* gen2 tx power off */
	mx6dis_hdmi_phy_gen2_txpwron(hdmi, 0);

	/* gen2 pddq */
	mx6dis_hdmi_phy_gen2_pddq(hdmi, 1);

	/* PHY reset */
	hdmi_writeb(hdmi, HDMI_MC_PHYRSTZ_DEASSERT, HDMI_MC_PHYRSTZ);
	hdmi_writeb(hdmi, HDMI_MC_PHYRSTZ_ASSERT, HDMI_MC_PHYRSTZ);

	hdmi_writeb(hdmi, HDMI_MC_HEACPHY_RST_ASSERT, HDMI_MC_HEACPHY_RST);

	mx6dis_hdmi_phy_test_clear(hdmi, 1);
	hdmi_writeb(hdmi, HDMI_PHY_I2CM_SLAVE_ADDR_PHY_GEN2,
		    HDMI_PHY_I2CM_SLAVE_ADDR);
	mx6dis_hdmi_phy_test_clear(hdmi, 0);

	/* PLL/MPLL Cfg - always match on final entry */
	for (i = 0; mpll_config[i].mpixelclock != (~0UL); i++)
		if (hdmi->timings.bt.pixelclock <=
		    mpll_config[i].mpixelclock)
			break;

	mx6dis_hdmi_phy_i2c_write(hdmi, mpll_config[i].res[res_idx].cpce, 0x06);
	mx6dis_hdmi_phy_i2c_write(hdmi, mpll_config[i].res[res_idx].gmp, 0x15);

	for (i = 0; curr_ctrl[i].mpixelclock != (~0UL); i++)
		if (hdmi->timings.bt.pixelclock <=
		    curr_ctrl[i].mpixelclock)
			break;

	if (curr_ctrl[i].mpixelclock == (~0UL)) {
		dev_err(hdmi->dev, "Pixel clock %llu - unsupported by HDMI\n",
				hdmi->timings.bt.pixelclock);
		return -EINVAL;
	}

	/* CURRCTRL */
	mx6dis_hdmi_phy_i2c_write(hdmi, curr_ctrl[i].curr[res_idx], 0x10);

	mx6dis_hdmi_phy_i2c_write(hdmi, 0x0000, 0x13);  /* PLLPHBYCTRL */
	mx6dis_hdmi_phy_i2c_write(hdmi, 0x0006, 0x17);

	for (i = 0; sym_term[i].mpixelclock != (~0UL); i++)
		if (hdmi->timings.bt.pixelclock <=
		    sym_term[i].mpixelclock)
			break;

	/* RESISTANCE TERM 133Ohm Cfg */
	mx6dis_hdmi_phy_i2c_write(hdmi, sym_term[i].term, 0x19);  /* TXTERM */
	/* PREEMP Cgf 0.00 CKSYMTXCTRL*/
	mx6dis_hdmi_phy_i2c_write(hdmi, sym_term[i].sym_ctr, 0x09);

	/* TX/CK LVL 10 */
	mx6dis_hdmi_phy_i2c_write(hdmi, 0x01ad, 0x0E);  /* VLEVCTRL */
	/* REMOVE CLK TERM */
	mx6dis_hdmi_phy_i2c_write(hdmi, 0x8000, 0x05);  /* CKCALCTRL */

	mx6dis_hdmi_phy_enable_power(hdmi, 1);

	/* toggle TMDS enable */
	mx6dis_hdmi_phy_enable_tmds(hdmi, 0);
	mx6dis_hdmi_phy_enable_tmds(hdmi, 1);

	/* gen2 tx power on */
	mx6dis_hdmi_phy_gen2_txpwron(hdmi, 1);
	mx6dis_hdmi_phy_gen2_pddq(hdmi, 0);

	/*Wait for PHY PLL lock */
	msec = 5;
	do {
		val = hdmi_readb(hdmi, HDMI_PHY_STAT0) & HDMI_PHY_TX_PHY_LOCK;
		if (!val)
			break;

		if (msec == 0) {
			dev_err(hdmi->dev, "PHY PLL not locked\n");
			return -ETIMEDOUT;
		}

		udelay(1000);
		msec--;
	} while (1);

	return 0;
}

static void mx6dis_hdmi_initialize_ih_mutes(struct mx6dis_hdmi_state *hdmi)
{
	u8 ih_mute;

	/*
	 * Boot up defaults are:
	 * HDMI_IH_MUTE   = 0x03 (disabled)
	 * HDMI_IH_MUTE_* = 0x00 (enabled)
	 *
	 * Disable top level interrupt bits in HDMI block
	 */
	ih_mute = hdmi_readb(hdmi, HDMI_IH_MUTE) |
		  HDMI_IH_MUTE_MUTE_WAKEUP_INTERRUPT |
		  HDMI_IH_MUTE_MUTE_ALL_INTERRUPT;

	hdmi_writeb(hdmi, ih_mute, HDMI_IH_MUTE);

	/* by default mask all interrupts */
	hdmi_writeb(hdmi, 0xff, HDMI_VP_MASK);
	hdmi_writeb(hdmi, 0xff, HDMI_FC_MASK0);
	hdmi_writeb(hdmi, 0xff, HDMI_FC_MASK1);
	hdmi_writeb(hdmi, 0xff, HDMI_FC_MASK2);
	hdmi_writeb(hdmi, 0xff, HDMI_PHY_MASK0);
	hdmi_writeb(hdmi, 0xff, HDMI_PHY_I2CM_INT_ADDR);
	hdmi_writeb(hdmi, 0xff, HDMI_PHY_I2CM_CTLINT_ADDR);
	hdmi_writeb(hdmi, 0xff, HDMI_AUD_INT);
	hdmi_writeb(hdmi, 0xff, HDMI_AUD_SPDIFINT);
	hdmi_writeb(hdmi, 0xff, HDMI_AUD_HBR_MASK);
	hdmi_writeb(hdmi, 0xff, HDMI_GP_MASK);
	hdmi_writeb(hdmi, 0xff, HDMI_A_APIINTMSK);
	hdmi_writeb(hdmi, 0xff, HDMI_CEC_MASK);
	hdmi_writeb(hdmi, 0xff, HDMI_I2CM_INT);
	hdmi_writeb(hdmi, 0xff, HDMI_I2CM_CTLINT);

	/* Disable interrupts in the IH_MUTE_* registers */
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_FC_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_FC_STAT1);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_FC_STAT2);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_AS_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_PHY_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_I2CM_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_CEC_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_VP_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_I2CMPHY_STAT0);
	hdmi_writeb(hdmi, 0xff, HDMI_IH_MUTE_AHBDMAAUD_STAT0);

	/* Enable top level interrupt bits in HDMI block */
	ih_mute &= ~(HDMI_IH_MUTE_MUTE_WAKEUP_INTERRUPT |
		    HDMI_IH_MUTE_MUTE_ALL_INTERRUPT);
	hdmi_writeb(hdmi, ih_mute, HDMI_IH_MUTE);
}

static unsigned int mx6dis_hdmi_compute_n(unsigned int freq,
					  unsigned long pixel_clk,
					  unsigned int ratio)
{
	unsigned int n = (128 * freq) / 1000;

	switch (freq) {
	case 32000:
		if (pixel_clk == 25170000)
			n = (ratio == 150) ? 9152 : 4576;
		else if (pixel_clk == 27020000)
			n = (ratio == 150) ? 8192 : 4096;
		else if (pixel_clk == 74170000 || pixel_clk == 148350000)
			n = 11648;
		else
			n = 4096;
		break;

	case 44100:
		if (pixel_clk == 25170000)
			n = 7007;
		else if (pixel_clk == 74170000)
			n = 17836;
		else if (pixel_clk == 148350000)
			n = (ratio == 150) ? 17836 : 8918;
		else
			n = 6272;
		break;

	case 48000:
		if (pixel_clk == 25170000)
			n = (ratio == 150) ? 9152 : 6864;
		else if (pixel_clk == 27020000)
			n = (ratio == 150) ? 8192 : 6144;
		else if (pixel_clk == 74170000)
			n = 11648;
		else if (pixel_clk == 148350000)
			n = (ratio == 150) ? 11648 : 5824;
		else
			n = 6144;
		break;

	case 88200:
		n = mx6dis_hdmi_compute_n(44100, pixel_clk, ratio) * 2;
		break;

	case 96000:
		n = mx6dis_hdmi_compute_n(48000, pixel_clk, ratio) * 2;
		break;

	case 176400:
		n = mx6dis_hdmi_compute_n(44100, pixel_clk, ratio) * 4;
		break;

	case 192000:
		n = mx6dis_hdmi_compute_n(48000, pixel_clk, ratio) * 4;
		break;

	default:
		break;
	}

	return n;
}

static unsigned int mx6dis_hdmi_compute_cts(unsigned int freq,
					    unsigned long pixel_clk,
					    unsigned int ratio)
{
	unsigned int cts = 0;

	pr_debug("%s: freq: %d pixel_clk: %ld ratio: %d\n", __func__, freq,
		 pixel_clk, ratio);

	switch (freq) {
	case 32000:
		if (pixel_clk == 297000000) {
			cts = 222750;
			break;
		}
	case 48000:
	case 96000:
	case 192000:
		switch (pixel_clk) {
		case 25200000:
		case 27000000:
		case 54000000:
		case 74250000:
		case 148500000:
			cts = pixel_clk / 1000;
			break;
		case 297000000:
			cts = 247500;
			break;
		/*
		 * All other TMDS clocks are not supported by
		 * DWC_hdmi_tx. The TMDS clocks divided or
		 * multiplied by 1,001 coefficients are not
		 * supported.
		 */
		default:
			break;
		}
		break;
	case 44100:
	case 88200:
	case 176400:
		switch (pixel_clk) {
		case 25200000:
			cts = 28000;
			break;
		case 27000000:
			cts = 30000;
			break;
		case 54000000:
			cts = 60000;
			break;
		case 74250000:
			cts = 82500;
			break;
		case 148500000:
			cts = 165000;
			break;
		case 297000000:
			cts = 247500;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	if (ratio == 100)
		return cts;
	return (cts * ratio) / 100;
}

static void mx6dis_hdmi_disable_overflow_interrupts(
		struct mx6dis_hdmi_state *hdmi)
{
	hdmi_writeb(hdmi, HDMI_IH_MUTE_FC_STAT2_OVERFLOW_MASK,
		    HDMI_IH_MUTE_FC_STAT2);
}

static void mx6dis_hdmi_regenerate_cts(struct mx6dis_hdmi_state *hdmi,
				       unsigned int cts)
{
	/* Must be set/cleared first */
	hdmi_modb(hdmi, 0, HDMI_AUD_CTS3_CTS_MANUAL, HDMI_AUD_CTS3);

	hdmi_writeb(hdmi, cts & 0xff, HDMI_AUD_CTS1);
	hdmi_writeb(hdmi, (cts >> 8) & 0xff, HDMI_AUD_CTS2);
	hdmi_writeb(hdmi, ((cts >> 16) & HDMI_AUD_CTS3_AUDCTS19_16_MASK) |
		    HDMI_AUD_CTS3_CTS_MANUAL, HDMI_AUD_CTS3);
}

static void mx6dis_hdmi_set_clock_regenerator_n(struct mx6dis_hdmi_state *hdmi,
					 unsigned int value)
{
	hdmi_writeb(hdmi, value & 0xff, HDMI_AUD_N1);
	hdmi_writeb(hdmi, (value >> 8) & 0xff, HDMI_AUD_N2);
	hdmi_writeb(hdmi, (value >> 16) & 0x0f, HDMI_AUD_N3);

	/* nshift factor = 0 */
	hdmi_modb(hdmi, 0, HDMI_AUD_CTS3_N_SHIFT_MASK, HDMI_AUD_CTS3);
}

static void mx6dis_hdmi_set_clk_regenerator(struct mx6dis_hdmi_state *hdmi,
				     unsigned long pixel_clk)
{
	unsigned int clk_n, clk_cts;

	clk_n = mx6dis_hdmi_compute_n(hdmi->sample_rate, pixel_clk,
			       hdmi->ratio);
	clk_cts = mx6dis_hdmi_compute_cts(hdmi->sample_rate, pixel_clk,
				   hdmi->ratio);

	if (!clk_cts) {
		dev_dbg(hdmi->dev, "%s: pixel clock not supported: %lu\n",
			__func__, pixel_clk);
		return;
	}

	dev_dbg(hdmi->dev,
		 "%s: samplerate=%d  ratio=%d  pixelclk=%lu  N=%d cts=%d\n",
		__func__, hdmi->sample_rate, hdmi->ratio,
		pixel_clk, clk_n, clk_cts);

	mx6dis_hdmi_set_clock_regenerator_n(hdmi, clk_n);
	mx6dis_hdmi_regenerate_cts(hdmi, clk_cts);
}

/*
 * Set the clock with the actual configured timings
 */
static void mx6dis_hdmi_clk_regenerator_update_pixel_clock(
		struct mx6dis_hdmi_state *hdmi)
{
	mx6dis_hdmi_set_clk_regenerator(hdmi,
			hdmi->timings.bt.pixelclock);
}

/* Wait until we are registered to enable interrupts */
static int mx6dis_hdmi_fb_registered(struct mx6dis_hdmi_state *hdmi)
{
	hdmi_writeb(hdmi, HDMI_PHY_I2CM_INT_ADDR_DONE_POL,
		    HDMI_PHY_I2CM_INT_ADDR);

	hdmi_writeb(hdmi, HDMI_PHY_I2CM_CTLINT_ADDR_NAC_POL |
		    HDMI_PHY_I2CM_CTLINT_ADDR_ARBITRATION_POL,
		    HDMI_PHY_I2CM_CTLINT_ADDR);

	return 0;
}

static void mx6dis_hdmi_enable_audio_clk(struct mx6dis_hdmi_state *hdmi)
{
	hdmi_modb(hdmi, 0, HDMI_MC_CLKDIS_AUDCLK_DISABLE, HDMI_MC_CLKDIS);
}

static void mx6dis_hdmi_av_composer(struct mx6dis_hdmi_state *hdmi,
			     const struct v4l2_dv_timings *mode)
{
	u8 inv_val;
	int hblank, vblank;

	dev_dbg(hdmi->dev, "final pixclk = %llu\n", mode->bt.pixelclock);

	/* Set up HDMI_FC_INVIDCONF */
	inv_val = (hdmi->hdcp_enable ?
		HDMI_FC_INVIDCONF_HDCP_KEEPOUT_ACTIVE :
		HDMI_FC_INVIDCONF_HDCP_KEEPOUT_INACTIVE);

	inv_val |= (mode->bt.polarities & V4L2_DV_VSYNC_POS_POL ?
		HDMI_FC_INVIDCONF_VSYNC_IN_POLARITY_ACTIVE_HIGH :
		HDMI_FC_INVIDCONF_VSYNC_IN_POLARITY_ACTIVE_LOW);

	inv_val |= (mode->bt.polarities & V4L2_DV_HSYNC_POS_POL ?
		HDMI_FC_INVIDCONF_HSYNC_IN_POLARITY_ACTIVE_HIGH :
		HDMI_FC_INVIDCONF_HSYNC_IN_POLARITY_ACTIVE_LOW);

	/* TODO Check polarity of IPU data? */
	inv_val |= HDMI_FC_INVIDCONF_DE_IN_POLARITY_ACTIVE_HIGH;
			/*(vmode->mdataenablepolarity ?
		HDMI_FC_INVIDCONF_DE_IN_POLARITY_ACTIVE_HIGH :
		HDMI_FC_INVIDCONF_DE_IN_POLARITY_ACTIVE_LOW);*/

	inv_val |= (mode->bt.interlaced ?
			HDMI_FC_INVIDCONF_R_V_BLANK_IN_OSC_ACTIVE_HIGH :
			HDMI_FC_INVIDCONF_R_V_BLANK_IN_OSC_ACTIVE_LOW);

	inv_val |= (mode->bt.interlaced ?
		HDMI_FC_INVIDCONF_IN_I_P_INTERLACED :
		HDMI_FC_INVIDCONF_IN_I_P_PROGRESSIVE);

	inv_val |= HDMI_FC_INVIDCONF_DVI_MODEZ_HDMI_MODE;

	hdmi_writeb(hdmi, inv_val, HDMI_FC_INVIDCONF);

	/* Set up horizontal active pixel width */
	hdmi_writeb(hdmi, mode->bt.width >> 8, HDMI_FC_INHACTV1);
	hdmi_writeb(hdmi, mode->bt.width, HDMI_FC_INHACTV0);

	/* Set up vertical active lines */
	hdmi_writeb(hdmi, mode->bt.height >> 8, HDMI_FC_INVACTV1);
	hdmi_writeb(hdmi, mode->bt.height, HDMI_FC_INVACTV0);

	/* Set up horizontal blanking pixel region width */
	hblank = V4L2_DV_BT_BLANKING_WIDTH(&mode->bt);
	hdmi_writeb(hdmi, hblank >> 8, HDMI_FC_INHBLANK1);
	hdmi_writeb(hdmi, hblank, HDMI_FC_INHBLANK0);

	/* Set up vertical blanking pixel region width */
	vblank = V4L2_DV_BT_BLANKING_HEIGHT(&mode->bt);
	hdmi_writeb(hdmi, vblank, HDMI_FC_INVBLANK);

	/* Set up HSYNC active edge delay (HFrontporchin pixel clks) */
	hdmi_writeb(hdmi, mode->bt.hfrontporch >> 8, HDMI_FC_HSYNCINDELAY1);
	hdmi_writeb(hdmi, mode->bt.hfrontporch, HDMI_FC_HSYNCINDELAY0);

	/* Set up VSYNC active edge delay (VFrontporchin in lines) */
	hdmi_writeb(hdmi, mode->bt.vfrontporch, HDMI_FC_VSYNCINDELAY);

	/* Set up HSYNC active pulse length (in pixel clks) */
	hdmi_writeb(hdmi, mode->bt.hsync >> 8, HDMI_FC_HSYNCINWIDTH1);
	hdmi_writeb(hdmi, mode->bt.hsync, HDMI_FC_HSYNCINWIDTH0);

	/* Set up VSYNC active pulse length (in lines) */
	hdmi_writeb(hdmi, mode->bt.vsync, HDMI_FC_VSYNCINWIDTH);
}


static int mx6dis_hdmi_config_AVI(struct mx6dis_hdmi_state *hdmi)
{
	u8 val, pix_fmt, under_scan;
	u8 act_ratio, coded_ratio, colorimetry, ext_colorimetry = 0x00;
	bool aspect_16_9;

	aspect_16_9 = true; /* FIXME Take from timings?? */

	/* Check pixel format configured and colorimetry */
	if (hdmi->pixel_format.pix_format.pixelformat ==
			V4L2_PIX_FMT_YUV444) {
		pix_fmt = HDMI_FC_AVICONF0_PIX_FMT_YCBCR444;
		colorimetry = HDMI_FC_AVICONF1_COLORIMETRY_EXTENDED_INFO;

		if (hdmi->pixel_format.pix_format.colorspace ==
				V4L2_COLORSPACE_REC709)
			ext_colorimetry =
				HDMI_FC_AVICONF2_EXT_COLORIMETRY_XVYCC709;

	} else if (hdmi->pixel_format.pix_format.pixelformat ==
			V4L2_PIX_FMT_YVYU) {
		pix_fmt = HDMI_FC_AVICONF0_PIX_FMT_YCBCR422;
		ext_colorimetry = HDMI_FC_AVICONF2_EXT_COLORIMETRY_XVYCC601;

		if (hdmi->pixel_format.pix_format.colorspace ==
				V4L2_COLORSPACE_REC709)
			colorimetry = HDMI_FC_AVICONF1_COLORIMETRY_ITUR;
		else
			colorimetry = HDMI_FC_AVICONF1_COLORIMETRY_SMPTE;

	} else if (hdmi->pixel_format.pix_format.pixelformat ==
			V4L2_PIX_FMT_RGB24) {
		pix_fmt = HDMI_FC_AVICONF0_PIX_FMT_RGB;
		colorimetry = HDMI_FC_AVICONF1_COLORIMETRY_NO_DATA;
		ext_colorimetry = HDMI_FC_AVICONF2_EXT_COLORIMETRY_XVYCC601;
	} else {
		dev_err(hdmi->dev, "Pixel format %x not supported\n",
				hdmi->pixel_format.pix_format.pixelformat);
		return -EINVAL;
	}
	under_scan =  HDMI_FC_AVICONF0_SCAN_INFO_NODATA;

	/*
	 * Active format identification data is present in the AVI InfoFrame.
	 * Under scan info, no bar data
	 */
	val = pix_fmt | under_scan |
		HDMI_FC_AVICONF0_ACTIVE_FMT_INFO_PRESENT |
		HDMI_FC_AVICONF0_BAR_DATA_NO_DATA;

	hdmi_writeb(hdmi, val, HDMI_FC_AVICONF0);

	/* AVI Data Byte 2 -Set the Aspect Ratio */
	if (aspect_16_9) {
		act_ratio = HDMI_FC_AVICONF1_ACTIVE_ASPECT_RATIO_16_9;
		coded_ratio = HDMI_FC_AVICONF1_CODED_ASPECT_RATIO_16_9;
	} else {
		act_ratio = HDMI_FC_AVICONF1_ACTIVE_ASPECT_RATIO_4_3;
		coded_ratio = HDMI_FC_AVICONF1_CODED_ASPECT_RATIO_4_3;
	}

	val = colorimetry | coded_ratio | act_ratio;
	hdmi_writeb(hdmi, val, HDMI_FC_AVICONF1);

	/* AVI Data Byte 3 */
	val = HDMI_FC_AVICONF2_IT_CONTENT_NO_DATA | ext_colorimetry |
		HDMI_FC_AVICONF2_RGB_QUANT_DEFAULT |
		HDMI_FC_AVICONF2_SCALING_NONE;
	hdmi_writeb(hdmi, val, HDMI_FC_AVICONF2);

	/* AVI Data Byte 4. hdmi->vic is the index into CEA-861-E specification
	 * http://en.wikipedia.org/wiki/Extended_Display_Identification_Data */
	hdmi_writeb(hdmi, hdmi->vic, HDMI_FC_AVIVID);

	/* AVI Data Byte 5- set up input and output pixel repetition */
	val = ((1 <<
		HDMI_FC_PRCONF_INCOMING_PR_FACTOR_OFFSET) &
		HDMI_FC_PRCONF_INCOMING_PR_FACTOR_MASK) |
		((0 <<
		HDMI_FC_PRCONF_OUTPUT_PR_FACTOR_OFFSET) &
		HDMI_FC_PRCONF_OUTPUT_PR_FACTOR_MASK);
	hdmi_writeb(hdmi, val, HDMI_FC_PRCONF);

	/* IT Content and quantization range = don't care */
	val = HDMI_FC_AVICONF3_IT_CONTENT_TYPE_GRAPHICS |
		HDMI_FC_AVICONF3_QUANT_RANGE_LIMITED;
	hdmi_writeb(hdmi, val, HDMI_FC_AVICONF3);

	/* AVI Data Bytes 6-13 */
	hdmi_writeb(hdmi, 0, HDMI_FC_AVIETB0);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVIETB1);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVISBB0);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVISBB1);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVIELB0);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVIELB1);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVISRB0);
	hdmi_writeb(hdmi, 0, HDMI_FC_AVISRB1);

	return 0;
}

static void mx6dis_hdmi_tx_hdcp_config(struct mx6dis_hdmi_state *hdmi)
{
	u8 de;

	if (hdmi->timings.bt.polarities)
		de = HDMI_A_VIDPOLCFG_DATAENPOL_ACTIVE_HIGH;
	else
		de = HDMI_A_VIDPOLCFG_DATAENPOL_ACTIVE_LOW;

	/* disable rx detect */
	hdmi_modb(hdmi, HDMI_A_HDCPCFG0_RXDETECT_DISABLE,
		  HDMI_A_HDCPCFG0_RXDETECT_MASK, HDMI_A_HDCPCFG0);

	hdmi_modb(hdmi, de, HDMI_A_VIDPOLCFG_DATAENPOL_MASK, HDMI_A_VIDPOLCFG);

	hdmi_modb(hdmi, HDMI_A_HDCPCFG1_ENCRYPTIONDISABLE_DISABLE,
		  HDMI_A_HDCPCFG1_ENCRYPTIONDISABLE_MASK, HDMI_A_HDCPCFG1);
}

static void mx6dis_hdmi_enable_overflow_interrupts(
		struct mx6dis_hdmi_state *hdmi)
{
	hdmi_writeb(hdmi, 0, HDMI_FC_MASK2);
	hdmi_writeb(hdmi, 0, HDMI_IH_MUTE_FC_STAT2);
}

/* Workaround to clear the overflow condition */
static void mx6dis_hdmi_clear_overflow(struct mx6dis_hdmi_state *hdmi)
{
	int count;
	u8 val;

	/* TMDS software reset */
	hdmi_writeb(hdmi, (u8)~HDMI_MC_SWRSTZ_TMDSSWRST_REQ, HDMI_MC_SWRSTZ);

	val = hdmi_readb(hdmi, HDMI_FC_INVIDCONF);

	for (count = 0; count < 4; count++)
		hdmi_writeb(hdmi, val, HDMI_FC_INVIDCONF);
}

/*
 * this submodule is responsible for the video data synchronization.
 * for example, for RGB 4:4:4 input, the data map is defined as
 *			pin{47~40} <==> R[7:0]
 *			pin{31~24} <==> G[7:0]
 *			pin{15~8}  <==> B[7:0]
 */
static void mx6dis_hdmi_video_sample(struct mx6dis_hdmi_state *hdmi)
{
	struct v4l2_pix_format *pix_format = &hdmi->pixel_format.pix_format;
	int color_format = 0;
	u8 val;

	if (pix_format->pixelformat == V4L2_PIX_FMT_RGB24) {
		if (hdmi->pixel_format.depth == 8)
			color_format = 0x01;
		else if (hdmi->pixel_format.depth == 10)
			color_format = 0x03;
		else if (hdmi->pixel_format.depth == 12)
			color_format = 0x05;
		else if (hdmi->pixel_format.depth == 16)
			color_format = 0x07;
		else
			return;
	} else if (pix_format->pixelformat == V4L2_PIX_FMT_YUV444) {
		if (hdmi->pixel_format.depth == 8)
			color_format = 0x09;
		else if (hdmi->pixel_format.depth == 10)
			color_format = 0x0B;
		else if (hdmi->pixel_format.depth == 12)
			color_format = 0x0D;
		else if (hdmi->pixel_format.depth == 16)
			color_format = 0x0F;
		else
			return;
	} else if (pix_format->pixelformat == V4L2_PIX_FMT_YVYU) {
		if (hdmi->pixel_format.depth == 8)
			color_format = 0x16;
		else if (hdmi->pixel_format.depth == 10)
			color_format = 0x14;
		else if (hdmi->pixel_format.depth == 12)
			color_format = 0x12;
		else
			return;
	}

	val = HDMI_TX_INVID0_INTERNAL_DE_GENERATOR_DISABLE |
		((color_format << HDMI_TX_INVID0_VIDEO_MAPPING_OFFSET) &
		HDMI_TX_INVID0_VIDEO_MAPPING_MASK);
	hdmi_writeb(hdmi, val, HDMI_TX_INVID0);

	/* Enable TX stuffing: When DE is inactive, fix the output data to 0 */
	val = HDMI_TX_INSTUFFING_BDBDATA_STUFFING_ENABLE |
		HDMI_TX_INSTUFFING_RCRDATA_STUFFING_ENABLE |
		HDMI_TX_INSTUFFING_GYDATA_STUFFING_ENABLE;
	hdmi_writeb(hdmi, val, HDMI_TX_INSTUFFING);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_GYDATA0);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_GYDATA1);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_RCRDATA0);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_RCRDATA1);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_BCBDATA0);
	hdmi_writeb(hdmi, 0x0, HDMI_TX_BCBDATA1);
}


/*
 * HDMI video packetizer is used to packetize the data.
 * for example, if input is YCC422 mode or repeater is used,
 * data should be repacked this module can be bypassed.
 */
static void mx6dis_hdmi_video_packetize(struct mx6dis_hdmi_state *hdmi)
{
	unsigned int color_depth = 0;
	unsigned int remap_size = HDMI_VP_REMAP_YCC422_16bit;
	unsigned int output_select = HDMI_VP_CONF_OUTPUT_SELECTOR_PP;
	struct v4l2_pix_format *pix_format = &hdmi->pixel_format.pix_format;
	u8 val, vp_conf;

	if (pix_format->pixelformat == V4L2_PIX_FMT_RGB24 ||
			pix_format->pixelformat == V4L2_PIX_FMT_YUV444) {
		if (!hdmi->pixel_format.depth) {
			output_select = HDMI_VP_CONF_OUTPUT_SELECTOR_BYPASS;
		} else if (hdmi->pixel_format.depth == 8) {
			color_depth = 4;
			output_select = HDMI_VP_CONF_OUTPUT_SELECTOR_BYPASS;
		} else if (hdmi->pixel_format.depth == 10) {
			color_depth = 5;
		} else if (hdmi->pixel_format.depth == 12) {
			color_depth = 6;
		} else if (hdmi->pixel_format.depth == 16) {
			color_depth = 7;
		} else {
			return;
		}
	} else if (pix_format->pixelformat == V4L2_PIX_FMT_YVYU) {
		if (!hdmi->pixel_format.depth ||
				hdmi->pixel_format.depth == 8)
			remap_size = HDMI_VP_REMAP_YCC422_16bit;
		else if (hdmi->pixel_format.depth == 10)
			remap_size = HDMI_VP_REMAP_YCC422_20bit;
		else if (hdmi->pixel_format.depth == 12)
			remap_size = HDMI_VP_REMAP_YCC422_24bit;
		else
			return;
		output_select = HDMI_VP_CONF_OUTPUT_SELECTOR_YCC422;
	} else
		return;


	/* set the packetizer registers */
	val = ((color_depth << HDMI_VP_PR_CD_COLOR_DEPTH_OFFSET) &
		HDMI_VP_PR_CD_COLOR_DEPTH_MASK);
	hdmi_writeb(hdmi, val, HDMI_VP_PR_CD);

	hdmi_modb(hdmi, HDMI_VP_STUFF_PR_STUFFING_STUFFING_MODE,
		  HDMI_VP_STUFF_PR_STUFFING_MASK, HDMI_VP_STUFF);

	/* Data from pixel repeater block */
	/*if (hdmi_data->pix_repet_factor > 1) {
		vp_conf = HDMI_VP_CONF_PR_EN_ENABLE |
			  HDMI_VP_CONF_BYPASS_SELECT_PIX_REPEATER;
	} else { */
	/* data from packetizer block */
	vp_conf = HDMI_VP_CONF_PR_EN_DISABLE |
			HDMI_VP_CONF_BYPASS_SELECT_VID_PACKETIZER;

	hdmi_modb(hdmi, vp_conf,
		  HDMI_VP_CONF_PR_EN_MASK |
		  HDMI_VP_CONF_BYPASS_SELECT_MASK, HDMI_VP_CONF);

	hdmi_modb(hdmi, 1 << HDMI_VP_STUFF_IDEFAULT_PHASE_OFFSET,
		  HDMI_VP_STUFF_IDEFAULT_PHASE_MASK, HDMI_VP_STUFF);

	hdmi_writeb(hdmi, remap_size, HDMI_VP_REMAP);

	if (output_select == HDMI_VP_CONF_OUTPUT_SELECTOR_PP) {
		vp_conf = HDMI_VP_CONF_BYPASS_EN_DISABLE |
			  HDMI_VP_CONF_PP_EN_ENABLE |
			  HDMI_VP_CONF_YCC422_EN_DISABLE;
	} else if (output_select == HDMI_VP_CONF_OUTPUT_SELECTOR_YCC422) {
		vp_conf = HDMI_VP_CONF_BYPASS_EN_DISABLE |
			  HDMI_VP_CONF_PP_EN_DISABLE |
			  HDMI_VP_CONF_YCC422_EN_ENABLE;
	} else if (output_select == HDMI_VP_CONF_OUTPUT_SELECTOR_BYPASS) {
		vp_conf = HDMI_VP_CONF_BYPASS_EN_ENABLE |
			  HDMI_VP_CONF_PP_EN_DISABLE |
			  HDMI_VP_CONF_YCC422_EN_DISABLE;
	} else {
		return;
	}

	hdmi_modb(hdmi, vp_conf,
		  HDMI_VP_CONF_BYPASS_EN_MASK | HDMI_VP_CONF_PP_EN_ENMASK |
		  HDMI_VP_CONF_YCC422_EN_MASK, HDMI_VP_CONF);

	hdmi_modb(hdmi, HDMI_VP_STUFF_PP_STUFFING_STUFFING_MODE |
			HDMI_VP_STUFF_YCC422_STUFFING_STUFFING_MODE,
		  HDMI_VP_STUFF_PP_STUFFING_MASK |
		  HDMI_VP_STUFF_YCC422_STUFFING_MASK, HDMI_VP_STUFF);

	hdmi_modb(hdmi, output_select, HDMI_VP_CONF_OUTPUT_SELECTOR_MASK,
		  HDMI_VP_CONF);
}

static int mx6dis_hdmi_phy_init(struct mx6dis_hdmi_state *hdmi)
{
	int i, ret;
	bool cscon = false;

	/* TODO check csc whether needed activated in HDMI mode */
	/*cscon = (is_color_space_conversion(hdmi) &&
			!hdmi->hdmi_data.video_mode.mdvi);*/

	/* HDMI Phy spec says to do the phy initialization sequence twice */
	for (i = 0; i < 2; i++) {
		mx6dis_hdmi_phy_sel_data_en_pol(hdmi, 1);
		mx6dis_hdmi_phy_sel_interface_control(hdmi, 0);
		mx6dis_hdmi_phy_enable_tmds(hdmi, 0);
		mx6dis_hdmi_phy_enable_power(hdmi, 0);

		/* Enable CSC */
		ret = mx6dis_hdmi_phy_configure(hdmi, 0, 8, cscon);
		if (ret)
			return ret;
	}

	hdmi->phy_enabled = true;
	return 0;
}

static void mx6dis_hdmi_enable_video_path(struct mx6dis_hdmi_state *hdmi)
{
	u8 clkdis;

	/* control period minimum duration */
	hdmi_writeb(hdmi, 12, HDMI_FC_CTRLDUR);
	hdmi_writeb(hdmi, 32, HDMI_FC_EXCTRLDUR);
	hdmi_writeb(hdmi, 1, HDMI_FC_EXCTRLSPAC);

	/* Set to fill TMDS data channels */
	hdmi_writeb(hdmi, 0x0B, HDMI_FC_CH0PREAM);
	hdmi_writeb(hdmi, 0x16, HDMI_FC_CH1PREAM);
	hdmi_writeb(hdmi, 0x21, HDMI_FC_CH2PREAM);

	/* Enable pixel clock and tmds data path */
	clkdis = 0x7F;
	clkdis &= ~HDMI_MC_CLKDIS_PIXELCLK_DISABLE;
	hdmi_writeb(hdmi, clkdis, HDMI_MC_CLKDIS);

	clkdis &= ~HDMI_MC_CLKDIS_TMDSCLK_DISABLE;
	hdmi_writeb(hdmi, clkdis, HDMI_MC_CLKDIS);

	/* TODO Enable csc path */
	/*if (is_color_space_conversion(hdmi)) {
		clkdis &= ~HDMI_MC_CLKDIS_CSCCLK_DISABLE;
		hdmi_writeb(hdmi, clkdis, HDMI_MC_CLKDIS);
	}*/
}

static int mx6dis_hdmi_setup(struct mx6dis_hdmi_state *hdmi)
{
	int ret;

	mx6dis_hdmi_disable_overflow_interrupts(hdmi);

	hdmi->hdcp_enable = 0;

	update_format_from_timings(&hdmi->pixel_format.pix_format,
				   &hdmi->timings);

	/* HDMI Initialization Step B.1 */
	mx6dis_hdmi_av_composer(hdmi, &hdmi->timings);

	/* HDMI Initializateion Step B.2 */
	ret = mx6dis_hdmi_phy_init(hdmi);
	if (ret)
		return ret;

	/* HDMI Initialization Step B.3 */
	mx6dis_hdmi_enable_video_path(hdmi);

	/* TODO only HDMI for the moment not for DVI mode
	if (hdmi->hdmi_data.video_mode.mdvi) {
		dev_info(hdmi->dev, "%s DVI mode\n", __func__);
	} else {*/
	dev_dbg(hdmi->dev, "%s CEA mode\n", __func__);

	/* HDMI Initialization Step E - Configure audio */
	mx6dis_hdmi_clk_regenerator_update_pixel_clock(hdmi);
	mx6dis_hdmi_enable_audio_clk(hdmi);

	/* HDMI Initialization Step F - Configure AVI InfoFrame */
	ret = mx6dis_hdmi_config_AVI(hdmi);
	if (ret)
		return ret;

	mx6dis_hdmi_video_packetize(hdmi);
	/*hdmi_video_csc(hdmi);*/
	mx6dis_hdmi_video_sample(hdmi);
	mx6dis_hdmi_tx_hdcp_config(hdmi);

	mx6dis_hdmi_clear_overflow(hdmi);
	if (hdmi->cable_plugin)
		mx6dis_hdmi_enable_overflow_interrupts(hdmi);

	return 0;
}

static void mx6dis_hdmi_phy_disable(struct mx6dis_hdmi_state *hdmi)
{
	if (!hdmi->phy_enabled)
		return;

	mx6dis_hdmi_phy_enable_tmds(hdmi, 0);
	mx6dis_hdmi_phy_enable_power(hdmi, 0);

	hdmi->phy_enabled = false;
}

static void mx6dis_hdmi_poweron(struct mx6dis_hdmi_state *hdmi)
{
	mx6dis_hdmi_setup(hdmi);
}

static void mx6dis_hdmi_poweroff(struct mx6dis_hdmi_state *hdmi)
{
	mx6dis_hdmi_phy_disable(hdmi);
}

static irqreturn_t mx6dis_hdmi_hardirq(int irq, void *dev_id)
{
	struct mx6dis_hdmi_state *hdmi = dev_id;
	u8 intr_stat;

	intr_stat = hdmi_readb(hdmi, HDMI_IH_PHY_STAT0);

	if (intr_stat)
		hdmi_writeb(hdmi, ~0, HDMI_IH_MUTE_PHY_STAT0);

	return intr_stat ? IRQ_WAKE_THREAD : IRQ_NONE;
}

static irqreturn_t mx6dis_hdmi_plug_interrupt(int irq, void *dev_id)
{
	struct mx6dis_hdmi_state *hdmi = dev_id;
	u8 intr_stat;
	u8 phy_int_pol;
	struct v4l2_event events = {
		.type = V4L2_EVENT_SOURCE_CHANGE,
		.u.src_change.changes = 0,
	};

	intr_stat = hdmi_readb(hdmi, HDMI_IH_PHY_STAT0);
	phy_int_pol = hdmi_readb(hdmi, HDMI_PHY_POL0);

	if (intr_stat & HDMI_IH_PHY_STAT0_HPD) {

		if (phy_int_pol & HDMI_PHY_HPD) {
			v4l2_dbg(1, debug, &hdmi->sd, "EVENT=plugin\n");

			hdmi_modb(hdmi, 0, HDMI_PHY_HPD, HDMI_PHY_POL0);
			hdmi->cable_plugin = true;

			v4l2_subdev_notify(&hdmi->sd, MX6DIS_HDMI_HOTPLUG,
					   &events);

		} else {
			v4l2_dbg(1, debug, &hdmi->sd, "EVENT=plugout\n");

			hdmi_modb(hdmi, HDMI_PHY_HPD, HDMI_PHY_HPD,
				  HDMI_PHY_POL0);
			mx6dis_hdmi_poweroff(hdmi);
			hdmi->cable_plugin = false;
		}
	}

	hdmi_writeb(hdmi, intr_stat, HDMI_IH_PHY_STAT0);

	/* Unmute Hot Plugin Interrupt */
	hdmi_writeb(hdmi, ~HDMI_IH_PHY_STAT0_HPD, HDMI_IH_MUTE_PHY_STAT0);

	return IRQ_HANDLED;
}

/***********************************/
/* V4L2 Subdevice Core Operations  */
/***********************************/
static int mx6dis_hdmi_log_status(struct v4l2_subdev *sd)
{
	v4l2_info(sd, "---------------------------");
	v4l2_info(sd, "  HDMI Transmitter Status  ");
	v4l2_info(sd, "---------------------------");
	return 0;
}

/***********************************/
/* V4L2 Subdevice Video Operations  */
/***********************************/
static int mx6dis_hdmi_s_stream(struct v4l2_subdev *sd, int enable)
{
	struct mx6dis_hdmi_state *hdmi = v4l2_get_subdevdata(sd);

	dev_dbg(sd->dev, "Subdev video operation streaming %s\n",
		  enable ? "on" : "off");

	if (enable)
		mx6dis_hdmi_poweron(hdmi);
	else
		mx6dis_hdmi_poweroff(hdmi);
	return 0;
}


/*
 * Set dv_timings. check if the timings is suitable for the device that
 * accepts timings from CEA-861-E specification
 */
static int mx6dis_hdmi_s_dv_timings(struct v4l2_subdev *sd,
		struct v4l2_dv_timings *timings)
{
	struct mx6dis_hdmi_state *hdmi = v4l2_get_subdevdata(sd);

	v4l2_dbg(1, debug, sd, "Setting dv timings\n");

	if (!timings)
		return -EINVAL;

	if (v4l2_match_dv_timings(&hdmi->timings, timings, 0)) {
		v4l2_dbg(1, debug, sd, "%s: no change\n", __func__);
		return 0;
	}

	/*
	 * Check correspondance with the supported timings and get the
	 * index for CEA-861-E which will be used on mx6dis_hdmi_config_AVI
	 */
	if (!find_cea861e_index_timings(hdmi, timings)) {
		v4l2_dbg(1, debug, sd, "%s: timings not allowed\n", __func__);
		return -ENODATA;
	}

	hdmi->timings = *timings;
	update_format_from_timings(&hdmi->pixel_format.pix_format,
				   &hdmi->timings);

	if (debug >= 1)
		v4l2_print_dv_timings(sd->name, "hdmi_s_dv_timings: ",
				      timings, true);
	return 0;
}

static int mx6dis_hdmi_g_dv_timings(struct v4l2_subdev *sd,
		struct v4l2_dv_timings *timings)
{
	struct mx6dis_hdmi_state *hdmi = v4l2_get_subdevdata(sd);

	*timings = hdmi->timings;
	return 0;
}

static const struct v4l2_subdev_core_ops mx6dis_hdmi_core_ops = {
	.log_status = mx6dis_hdmi_log_status,
};

static const struct v4l2_subdev_video_ops mx6dis_hdmi_video_ops = {
	.s_stream = mx6dis_hdmi_s_stream,
	.s_dv_timings = mx6dis_hdmi_s_dv_timings,
	.g_dv_timings = mx6dis_hdmi_g_dv_timings,
};

/* V4l2 Subdev operations */
static const struct v4l2_subdev_ops mx6dis_hdmi_ops = {
	.core = &mx6dis_hdmi_core_ops,
	.video = &mx6dis_hdmi_video_ops,
};

static int mx6dis_hdmi_parse_dt(struct platform_device *pdev)
{
	struct mx6dis_hdmi_state *hdmi = platform_get_drvdata(pdev);
	struct device_node *of, *ddc_node;
	int ret, irq;

	if (!hdmi)
		return -EINVAL;

	of = hdmi->dev->of_node;
	hdmi->regmap = syscon_regmap_lookup_by_phandle(of, "gpr");
	if (IS_ERR(hdmi->regmap)) {
		v4l2_err(&hdmi->sd, "Unable to get gpr\n");
		return PTR_ERR(hdmi->regmap);
	}

	v4l2_dbg(1, debug, &hdmi->sd, "Get regmap from gpr phandle\n");

	ddc_node = of_parse_phandle(of, "ddc-i2c-bus", 0);
	if (ddc_node) {
		hdmi->ddc = of_find_i2c_adapter_by_node(ddc_node);
		of_node_put(ddc_node);
		if (!hdmi->ddc) {
			v4l2_err(&hdmi->sd, "failed to read ddc node\n");
			return -EPROBE_DEFER;
		}
		v4l2_dbg(1, debug, &hdmi->sd, "ddc property found\n");
	} else {
		v4l2_dbg(1, debug, &hdmi->sd, "No ddc property found\n");
	}

	hdmi->regs = devm_ioremap_resource(hdmi->dev, hdmi->iores);
	if (IS_ERR(hdmi->regs)) {
		v4l2_err(&hdmi->sd, "Fail getting ioremap resources\n");
		return PTR_ERR(hdmi->regs);
	}
	v4l2_dbg(1, debug, &hdmi->sd, "Configured iomem map\n");

	hdmi->isfr_clk = devm_clk_get(hdmi->dev, "isfr");
	if (IS_ERR(hdmi->isfr_clk)) {
		v4l2_err(&hdmi->sd, "Unable to get HDMI isfr clk\n");
		return PTR_ERR(hdmi->isfr_clk);
	}
	v4l2_dbg(1, debug, &hdmi->sd, "Configured isfr clock\n");

	ret = clk_prepare_enable(hdmi->isfr_clk);
	if (ret) {
		v4l2_err(&hdmi->sd, "Cannot enable HDMI isfr clock: %d\n", ret);
		return ret;
	}
	v4l2_dbg(1, debug, &hdmi->sd, "Enable isfr clock\n");

	hdmi->iahb_clk = devm_clk_get(hdmi->dev, "iahb");
	if (IS_ERR(hdmi->iahb_clk)) {
		ret = PTR_ERR(hdmi->iahb_clk);
		v4l2_err(&hdmi->sd, "Unable to get HDMI iahb clk: %d\n", ret);
		goto err_isfr;
	}
	v4l2_dbg(1, debug, &hdmi->sd, "Configured iahb clock\n");

	ret = clk_prepare_enable(hdmi->iahb_clk);
	if (ret) {
		v4l2_err(&hdmi->sd, "Cannot enable HDMI iahb clock: %d\n", ret);
		goto err_isfr;
	}
	v4l2_dbg(1, debug, &hdmi->sd, "Enable iahb clock\n");

	/* Product and revision IDs */
	v4l2_dbg(1, debug, &hdmi->sd,
		 "Detected HDMI controller 0x%x:0x%x:0x%x:0x%x\n",
		 hdmi_readb(hdmi, HDMI_DESIGN_ID),
		 hdmi_readb(hdmi, HDMI_REVISION_ID),
		 hdmi_readb(hdmi, HDMI_PRODUCT_ID0),
		 hdmi_readb(hdmi, HDMI_PRODUCT_ID1));

	/* Configure HDMI interrupts */
	ret = platform_get_irq(pdev, 0);
	if (!ret) {
		v4l2_err(&hdmi->sd, "No IRQ found dev %s\n",
				dev_name(&pdev->dev));
		goto err_iahb;
	}
	v4l2_dbg(1, debug, &hdmi->sd, "IRQ %d get\n", ret);

	irq = ret;
	ret = devm_request_threaded_irq(&pdev->dev, irq, mx6dis_hdmi_hardirq,
			mx6dis_hdmi_plug_interrupt, IRQF_SHARED,
			dev_name(&pdev->dev), hdmi);
	if (ret) {
		v4l2_err(&hdmi->sd, "Error requesting IRQ %d\n", irq);
		goto err_iahb;
	}

	mx6dis_hdmi_initialize_ih_mutes(hdmi);
	v4l2_dbg(1, debug, &hdmi->sd, "Initialize interrupt mute values\n");

	/*
	 * To prevent overflows in HDMI_IH_FC_STAT2, set the clk regenerator
	 * N and cts values before enabling phy. HDMI timings has been
	 * already configured
	 */
	mx6dis_hdmi_clk_regenerator_update_pixel_clock(hdmi);

	/*
	 * Configure registers related to HDMI Hot Plugin interrupt
	 * generation before registering IRQ.
	 */
	/* Set polarity positive, it will change each HPD Interrupt */
	hdmi_writeb(hdmi, HDMI_PHY_HPD, HDMI_PHY_POL0);

	/* Set state of HPD active */
	hdmi_writeb(hdmi, HDMI_IH_PHY_STAT0_HPD, HDMI_IH_PHY_STAT0);

	/* Unmask hot plugin mask */
	hdmi_writeb(hdmi, (u8)~HDMI_PHY_HPD, HDMI_PHY_MASK0);

	ret = mx6dis_hdmi_fb_registered(hdmi);
	if (ret)
		goto err_iahb;

	/* Unmute Hot Plugin interrupts */
	hdmi_writeb(hdmi, ~HDMI_IH_PHY_STAT0_HPD, HDMI_IH_MUTE_PHY_STAT0);

	return 0;

err_iahb:
	clk_disable_unprepare(hdmi->iahb_clk);
err_isfr:
	clk_disable_unprepare(hdmi->isfr_clk);

	return ret;
}

static const struct of_device_id mx6dis_hdmi_dt_ids[] = {
	{ .compatible = "fsl,imx6q-hdmi",
	  .data = &imx6q_hdmi_drv_data
	},
	{},
};
MODULE_DEVICE_TABLE(of, mx6dis_hdmi_dt_ids);

static int mx6dis_hdmi_probe(struct platform_device *pdev)
{
	const struct of_device_id *match;
	struct mx6dis_hdmi_state *hdmi;
	struct v4l2_subdev *sd;
	int ret;

	if (!pdev->dev.of_node)
		return -ENODEV;

	hdmi = devm_kzalloc(&pdev->dev, sizeof(*hdmi), GFP_KERNEL);
	if (!hdmi)
		return -ENOMEM;

	match = of_match_node(mx6dis_hdmi_dt_ids, pdev->dev.of_node);
	if (!match) {
		dev_err(&pdev->dev, "There is not compatible data for %s\n",
				pdev->dev.of_node->name);
		return -ENOMEM;
	}

	/* Configure v4l2 subdev */
	sd = &hdmi->sd;
	v4l2_subdev_init(sd, &mx6dis_hdmi_ops);
	sd->owner = pdev->dev.driver->owner;
	sd->dev = &pdev->dev;
	sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE | V4L2_SUBDEV_FL_HAS_EVENTS;
	snprintf(sd->name, sizeof(sd->name), "%s", pdev->name);
	v4l2_set_subdevdata(sd, hdmi);

	/* Configure hdmi default values */
	hdmi->plat_data = match->data;
	hdmi->dev = &pdev->dev;
	hdmi->sample_rate = 48000;
	hdmi->ratio = 100;
	hdmi->write = mx6dis_hdmi_writeb;
	hdmi->read = mx6dis_hdmi_readb;

	hdmi->irq = platform_get_irq(pdev, 0);
	if (hdmi->irq < 0)
		return hdmi->irq;

	hdmi->iores = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!hdmi->iores)
		return -ENXIO;

	/* Setting other defaults */
	hdmi->timings = default_mode;
	hdmi->pixel_format.pix_format.pixelformat = V4L2_PIX_FMT_RGB24;
	hdmi->pixel_format.pix_format.colorspace = V4L2_COLORSPACE_REC709;
	hdmi->pixel_format.depth = 8; /* From dw_hdmi */
	hdmi->vic = 19; /* CEA/EIA-861E 720p50*/
	update_format_from_timings(&hdmi->pixel_format.pix_format,
				   &hdmi->timings);

	platform_set_drvdata(pdev, hdmi);

	ret = mx6dis_hdmi_parse_dt(pdev);
	if (ret) {
		v4l2_err(&hdmi->sd, "Unable to parse HDMI node\n");
		return ret;
	}

	ret = v4l2_async_register_subdev(sd);
	if (ret)
		return ret;

	v4l2_info(&hdmi->sd, "Device %s probed\n", dev_name(&pdev->dev));
	return 0;
}

static int mx6dis_hdmi_remove(struct platform_device *pdev)
{
	dev_info(&pdev->dev, "Removing dev%s\n", dev_name(&pdev->dev));
	return 0;
}

static struct platform_driver mx6dis_hdmi_platform_driver = {
	.probe  = mx6dis_hdmi_probe,
	.remove = mx6dis_hdmi_remove,
	.driver = {
		.name = DRIVER_NAME,
		.of_match_table = mx6dis_hdmi_dt_ids,
	},
};

module_platform_driver(mx6dis_hdmi_platform_driver);

MODULE_AUTHOR("Pablo Anton <pablo.anton@veo-labs.com>");
MODULE_DESCRIPTION("IMX6 Specific V4L2 HDMI TX Driver");
MODULE_LICENSE("GPL");
